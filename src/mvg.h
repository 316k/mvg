/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MVG_H_
#define MVG_H_

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <iostream>
#include "export.h"
#include "camera_params.h"

MVG_DECLSPEC bool clipLine(cv::Size     imgSize,
                           cv::Point2d &p1,
                           cv::Point2d &p2);

MVG_DECLSPEC void rank2MatrixSVDCorrection(const cv::Mat &src,
                                           cv::Mat       &dst);

MVG_DECLSPEC void skewMatrix(const cv::Mat &v,
                             cv::Mat       &dst);
template <typename T> void skewMatrix(const T *const v,
                                      T             *M);

MVG_DECLSPEC double pointDepth(const cv::Mat     &M,
                               const cv::Point3d &X);

// return the squared distance between point q and epipolar line F.p (supposes
// q'Fp = 0)
MVG_DECLSPEC double squaredDistanceToEpipolarLine(const cv::Mat     &F,
                                                  const cv::Point2d &p,
                                                  const cv::Point2d &q);
// return the squared distance between point p and epipolar line l
MVG_DECLSPEC double squaredDistanceToEpipolarLine(const cv::Mat     &l,
                                                  const cv::Point2d &p);
// return the distance between point q and epipolar line F.p (supposes q'Fp = 0)
// WARNING !!! this is a signed distance (can be negative)
MVG_DECLSPEC double distanceToEpipolarLine(const cv::Mat     &F,
                                           const cv::Point2d &p,
                                           const cv::Point2d &q);
// return the distance between point p and epipolar line l
// WARNING !!! this is a signed distance (can be negative)
MVG_DECLSPEC double distanceToEpipolarLine(const cv::Mat     &l,
                                           const cv::Point2d &p);

// true epipolar geometric distance : d(p,\hat{p})^2 + d(q,\hat{q})^2 where
// \hat{p} = [I|0]*X and \hat{q} = P*X
template <typename T>
T fundamentalGeometricDistance(const T *const P,
                               const T *const X,
                               const T *
                               const          p,
                               const T *const q);

// returns Sampson approximation to MLE of geometric distance for fundamental
// estimation
MVG_DECLSPEC double sampsonDistanceToEpipolarLine(const cv::Mat     &F,
                                                  const cv::Point2d &p,
                                                  const cv::Point2d &q);
template <typename T>
T sampsonDistanceToEpipolarLine(const T *const F,
                                const T *const p,
                                const T *
                                const          q);

// return the distance between point q and the projection of p through
// the homography H (supposes q x= H.p)
MVG_DECLSPEC double distanceToHomographyProjection(const cv::Mat     &H,
                                                   const cv::Point2d &p,
                                                   const cv::Point2d &q);
// return the squared distance between point q and the projection of p through
// the homography H (supposes q x= H.p)
MVG_DECLSPEC double squaredDistanceToHomographyProjection(const cv::Mat     &H,
                                                          const cv::Point2d &p,
                                                          const cv::Point2d &q);

// true homography geometric distance : d(p,\hat{p})^2 + d(q,H.\hat{p})^2
template <typename T>
T homographyGeometricDistance(const T *const H,
                              const T *const pe,
                              const T *
                              const          p,
                              const T *const q);

// returns Sampson approximation to MLE of geometric distance for homography
// estimation
MVG_DECLSPEC double sampsonDistanceToHomographyProjection(const cv::Mat     &H,
                                                          const cv::Point2d &p,
                                                          const cv::Point2d &q);

// true resection geometric distance : d(p, M*P)^2
template <typename T>
T resectionGeometricDistance(const T *const M,
                             const T *const P,
                             const T *const p);

// return the squared distance between point q and the projection of p through
// the camera matrix M (supposes q x= M.p)
MVG_DECLSPEC double squaredDistanceToCameraProjection(const cv::Mat     &M,
                                                      const cv::Point3d &p,
                                                      const
                                                      cv::Point2d       &q);
// returns Sampson approximation to MLE of geometric distance for resection
// estimation
MVG_DECLSPEC double sampsonDistanceToCameraProjection(const cv::Mat     &M,
                                                      const cv::Point3d &p,
                                                      const cv::Point2d &q);

/*
 * Implements the distance between fundamentals by sampling random points through
 * each images and computing the difference between the reprojection error with
 * the actual and computed fundamentals
 * Method by Laveau in Zhang's paper on epipolar geometry estimation
 */
MVG_DECLSPEC double distanceBetweenFundamentalMatrices(cv::Size       dim,
                                                       const cv::Mat &F1,
                                                       const cv::Mat &F2);

/*
 * returns two projectives cameras compatible with the epipolar geometry
 * encapsulated in the fundamental matrix F.
 * M1 = [I|0] and M2 = [e'X.F, e'] with e' the right epipole of F and e'X = [e']_X
 */
MVG_DECLSPEC void projectiveCamerasFromFundamentalMatrix(const cv::Mat &F,
                                                         cv::Mat       &M1,
                                                         cv::Mat       &M2);

/*
 * given epipolar geometry, compute rectifying camera transformations to
 * align epipolar lines, while minimizing geometric distorsions
 * align everything on the second camera
 * K[12], R[12] and t[12] are resp. new camera matrices, rotation and translations
 * H[12] is the combined transformation (including projection+rotation)
 * size[12] are optimal size chosen so that no pixels are left out
 */
// use K1 or K2 as the basis for the camera matrix (keep focals and centers, except adjust either cx
// or cy)
MVG_DECLSPEC void findRectificationTransform(const CameraGeometricParameters &cam1,
                                             const CameraGeometricParameters &cam2,
                                             bool                             useK1,
                                             cv::Mat                         &K1,
                                             cv::Mat                         &K2,
                                             cv::Mat                         &R1,
                                             cv::Mat                         &R2,
                                             cv::Mat                         &H1,
                                             cv::Mat                         &H2, // rectifying
                                                                                  // homographies
                                                                                  // in
                                                                                  // image space
                                                                                  // H1=K1.R.oK1^-1
                                                                                  // and
                                                                                  // H2=K2.R.oK2^-1
                                                                                  // with
                                                                                  // R=R1.oR1=R2.oR2
                                             cv::Size                        &s1,
                                             cv::Size                        &s2,
                                             cv::Size                        &commonSize,
                                             bool                             alignHorizontalEpipolarLines=
                                                 true); // align epipolar lines in the X or Y
                                                        // direction, you should ajust this based on
                                                        // the magnitude of the translation,
                                                        // choosing the one with
                                                        // the biggest magnitude.

/*
 * triangulate points so that their projection in each cameras is closest to the
 * given image points. each 3D point P returned is such that M_i.P \approx p_i
 * supposes there are no outliers in points given
 */
MVG_DECLSPEC void triangulatePoints(const std::vector<cv::Mat> &imagePoints,
                                    const std::vector<cv::Mat> &cameras,
                                    const cv::Mat              &visibilityMask,
                                    cv::Size                    camSize,
                                    cv::Mat                    &scenePoints);

/*
 * triangulate points so that their projection in each cameras is closest to the
 * given image points. each 3D point P returned is such that M_i.P \approx p_i
 * supposes there are no outliers in points given
 */
MVG_DECLSPEC void triangulatePoints(
    const std::vector<cv::Mat>                   &imagePoints,
    const std::vector<CameraGeometricParameters> &cameras,
    const cv::Mat                                &visibilityMask,
    cv::Size                                      camSize,
    cv::Mat                                      &scenePoints);

/*
 * triangulate points so that their projection in each cameras is closest to the
 * given image points. each 3D point P returned is such that M_i.P \approx p_i
 * supposes there are no outliers in points given
 * supposes full visibility mask
 * if completeCorrespondences is false, then only X or Y coordinates of points2
 * are given, and the remaining coordinate is computed by enforcing the epipolar constraint
 */
MVG_DECLSPEC void triangulatePoints(const cv::Mat                   &points1,
                                    const cv::Mat                   &points2,
                                    const CameraGeometricParameters &cam1,
                                    const CameraGeometricParameters &cam2,
                                    cv::Mat                         &scenePoints,
                                    bool                             completeCorrespondences=
                                        true);

/*
 * returns the normalization matrix that transform the points set so that its
 * centroid is \vec 0 and its scale is sqrt(dim) where dim is the dimension of
 * the points
 */
MVG_DECLSPEC cv::Mat normalizationMatrix(const cv::Mat &pts);

MVG_DECLSPEC void essentialFromFundamentalMatrix(const cv::Mat &F,
                                                 const cv::Mat &K1,
                                                 const cv::Mat &K2,
                                                 cv::Mat       &E);

MVG_DECLSPEC void poseFromEssentialMatrix(const cv::Mat &E,
                                          const cv::Mat &K1,
                                          const cv::Mat &K2,
                                          const cv::Mat &imagePoints1,
                                          const cv::Mat &imagePoints2,
                                          cv::Size       camSize1,
                                          cv::Size       camSize2,
                                          cv::Mat       &R,
                                          cv::Mat       &t);
/*
 * decomposes the matrix M so that M \approx K.[R|t]
 */
MVG_DECLSPEC bool decompositionKRT(const cv::Mat &M,
                                   cv::Mat       &K,
                                   cv::Mat       &R,
                                   cv::Mat       &t);

/*
 * find the exact homography matrix relating two sets of 4 points
 * the matrix returned will fit exactly the points
 */
MVG_DECLSPEC void findExactHomographyMatrix(const cv::Mat &points1,
                                            const cv::Mat &points2,
                                            cv::Mat       &hmatrix);

/*
 * find the exact fundamental matrix relating two sets of 7 points
 * the matrix returned will fit exactly the points
 */
MVG_DECLSPEC void findExactFundamentalMatrix(const cv::Mat &points1,
                                             const cv::Mat &points2,
                                             cv::Mat       &fmatrix);

/*
 * find the exact resection matrix relating two sets of 6 points
 * the matrix returned will fit exactly the points 5,5 points :
 * for the last point, x coordinates won't be used
 */
MVG_DECLSPEC void findExactResectionMatrix(const cv::Mat &points1,
                                           const cv::Mat &points2,
                                           cv::Mat       &mmatrix);

#include "mvg.tpp"

#endif // MVG_H_

