/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is an old version of OpenCV's cv::SVD that uses Lapack (until it is reinstated)
 */

#ifndef LSVD_H
#define LSVD_H

#include <opencv2/core/core.hpp>
#include <assert.h>
#include "export.h"

/*!
 *   Singular Value Decomposition class
 *
 *   The class is used to compute Singular Value Decomposition of a floating-point matrix and then
 *   use it to solve least-square problems, under-determined linear systems, invert matrices,
 *   compute condition numbers etc.
 *
 *   For a bit faster operation you can pass flags=SVD::MODIFY_A|... to modify the decomposed matrix
 *   when it is not necessarily to preserve it. If you want to compute condition number of a matrix
 *   or absolute value of its determinant - you do not need cv::SVD::u or cv::SVD::vt,
 *   so you can pass flags=SVD::NO_UV|... . Another flag cv::SVD::FULL_UV indicates that the
 *   full-size cv::SVD::u and cv::SVD::vt
 *   must be computed, which is not necessary most of the time.
 */
class MVG_DECLSPEC LSVD
{
    public:
        enum { MODIFY_A = 1, NO_UV = 2, FULL_UV = 4 };
        // ! the default constructor
        LSVD();
        // ! the constructor that performs cv::SVD
        LSVD(const cv::Mat &src,
             int            flags=0);
        // ! the operator that performs cv::SVD. The previously allocated cv::SVD::u, cv::SVD::w are
        // cv::SVD::vt are released.
        LSVD & operator()(const cv::Mat &src,
                          int            flags=0);

        LSVD(const LSVD &l);
        LSVD & operator=(const LSVD &l);

        ~LSVD();

        // ! decomposes matrix and stores the results to user-provided matrices
        static void compute(const cv::Mat &src,
                            cv::Mat       &w,
                            cv::Mat       &u,
                            cv::Mat       &vt,
                            int            flags=0);
        // ! computes singular values of a matrix
        static void compute(const cv::Mat &src,
                            cv::Mat       &w,
                            int            flags=0);
        // ! performs back substitution
        static void backSubst(const cv::Mat &w,
                              const cv::Mat &u,
                              const cv::Mat &vt,
                              const cv::Mat &rhs,
                              cv::Mat       &dst);
        // *INDENT-OFF*
        template<typename _Tp, int m, int n, int nm> static void compute(
            const cv::Matx<_Tp, m, n> &a,
            cv::Matx<_Tp, nm, 1> &w,
            cv::Matx<_Tp, m, nm> &u,
            cv::Matx<_Tp, n, nm> &vt);
        template<typename _Tp, int m, int n, int nm> static void compute(
            const cv::Matx<_Tp, m, n> &a,
            cv::Matx<_Tp, nm, 1> &w);
        template<typename _Tp, int m, int n, int nm, int nb> static void backSubst(
            const cv::Matx<_Tp, nm, 1> &w,
            const cv::Matx<_Tp, m, nm> &u,
            const cv::Matx<_Tp, n, nm> &vt,
            const cv::Matx<_Tp, m, nb> &rhs,
            cv::Matx<_Tp, n, nb> &dst);
        // *INDENT-ON*

        // ! finds dst = arg min_{|dst|=1} |m*dst|
        static void solveZ(const cv::Mat &src,
                           cv::Mat       &dst);
        // ! performs back substitution, so that dst is the solution or pseudo-solution of m*dst =
        // rhs, where m is the decomposed matrix
        void backSubst(const cv::Mat &rhs,
                       cv::Mat       &dst) const;

        cv::Mat & u();
        const cv::Mat & u() const;

        cv::Mat & w();
        const cv::Mat & w() const;

        cv::Mat & vt();
        const cv::Mat & vt() const;

    private:
        struct Private;
        Private *m_privImpl;

};

///////////////////////////////////////////// cv::SVD
// //////////////////////////////////////////////////////

template<typename _Tp, int m, int n, int nm> inline void
LSVD::compute(const cv::Matx<_Tp, m, n> &a,
              cv::Matx<_Tp, nm, 1> &w,
              cv::Matx<_Tp, m, nm> &u,
              cv::Matx<_Tp, n, nm> &vt)
{
    assert(nm == MIN(m, n));
    cv::Mat _a(a, false), _u(u, false), _w(w, false), _vt(vt, false);
    LSVD::compute(_a, _w, _u, _vt);
    CV_Assert(_w.data == static_cast<uchar *>(&w.val[0]) && _u.data ==
              static_cast<uchar *>(&u.val[0]) &&
              _vt.data == static_cast<uchar *>(&vt.val[0]));
}

template<typename _Tp, int m, int n, int nm> inline void
LSVD::compute(const cv::Matx<_Tp, m, n> &a,
              cv::Matx<_Tp, nm, 1> &w)
{
    assert(nm == MIN(m, n));
    cv::Mat _a(a, false), _w(w, false);
    LSVD::compute(_a, _w);
    CV_Assert(_w.data == static_cast<uchar *>(&w.val[0]));
}

template<typename _Tp, int m, int n, int nm, int nb> inline void
LSVD::backSubst(const cv::Matx<_Tp, nm, 1> &w,
                const cv::Matx<_Tp, m, nm> &u,
                const cv::Matx<_Tp, n, nm> &vt,
                const cv::Matx<_Tp, m, nb> &rhs,
                cv::Matx<_Tp, n, nb> &dst)
{
    assert(nm == MIN(m, n));
    cv::Mat _u(u, false), _w(w, false), _vt(vt, false),
    _rhs(rhs, false), _dst(dst, false);
    LSVD::backSubst(_w, _u, _vt, _rhs, _dst);
    CV_Assert(_dst.data == static_cast<uchar *>(&dst.val[0]));
}

// returns q and r such that q.r = s
void decompositionQR(const cv::Mat &s,
                     cv::Mat       &q,
                     cv::Mat       &r);
// returns r and q such that r.q = s
void decompositionRQ(const cv::Mat &s,
                     cv::Mat       &r,
                     cv::Mat       &q);

#endif
