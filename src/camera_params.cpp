/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "camera_params.h"
#include "mvg.h"
#include "logger.h"

using namespace std;
using namespace cv;

struct CameraGeometricParameters::Private
{
    Private();
    ~Private();

    Mat  K, R, t;
    Mat  coeffs;
    Size imageSize;
};

CameraGeometricParameters::Private::Private()
{
    /*
    K.create(3, 3, CV_64F); K = Scalar::all(0);
    R.create(3, 3, CV_64F); R = Scalar::all(0);
    t.create(3, 1, CV_64F); t = Scalar::all(0);
    coeffs.create(4, 1, CV_64F); coeffs = Scalar::all(0);
    */
}

CameraGeometricParameters::Private::~Private()
{ }

ostream & operator<<(ostream                         &os,
                     const CameraGeometricParameters &c)
{
    os << c.K() << endl;
    os << c.R() << endl;
    os << c.t() << endl;
    // os << c.M() << endl;    //redundant
    os << c.distorsionCoefficients() << endl;
    os << c.imageSize() << endl;

    return os;
}

CameraGeometricParameters::CameraGeometricParameters()
{
    m_privImpl = new Private();
}

CameraGeometricParameters::CameraGeometricParameters(const CameraGeometricParameters &c)
    : m_privImpl(new Private(*c.m_privImpl))
{ }

CameraGeometricParameters & CameraGeometricParameters::operator=(
    const CameraGeometricParameters &c)
{
    if (this != &c)
    {
        delete m_privImpl;
        m_privImpl = new Private(*c.m_privImpl);
    }

    return *this;
}

CameraGeometricParameters::CameraGeometricParameters(const Mat &M,
                                                     const Mat &coeffs,
                                                     Size       imageSize)
{
    m_privImpl = new Private();
    setMatrix(M);
    setDistorsionCoefficients(coeffs);
    setImageSize(imageSize);
}

CameraGeometricParameters::CameraGeometricParameters(const Mat &K,
                                                     const Mat &R,
                                                     const Mat &t,
                                                     const Mat &coeffs,
                                                     Size       imageSize)
{
    m_privImpl = new Private();
    setIntrinsicMatrix(K);
    setRotationMatrix(R);
    setTranslationVector(t);
    setDistorsionCoefficients(coeffs);
    setImageSize(imageSize);
}

CameraGeometricParameters::~CameraGeometricParameters()
{
    delete m_privImpl;
}

const Mat & CameraGeometricParameters::K() const
{
    return m_privImpl->K;
}

void CameraGeometricParameters::setIntrinsicMatrix(const Mat &K)
{
    if ((K.rows != 3) || (K.cols != 3) || (K.depth() != CV_64F) || (K.channels() != 1))
    {
        logWarning() << "Given K is not a 3x3 64bit float matrix, ignoring.";

        return;
    }

    m_privImpl->K.create(3, 3, CV_64F);
    K.copyTo(m_privImpl->K);
}

const Mat & CameraGeometricParameters::R() const
{
    return m_privImpl->R;
}

void CameraGeometricParameters::setRotationMatrix(const Mat &R)
{
    if ((R.rows != 3) || (R.cols != 3) || (R.depth() != CV_64F) || (R.channels() != 1))
    {
        logWarning() << "Given R is not a 3x3 64bit float matrix, ignoring.";

        return;
    }
    m_privImpl->R.create(3, 3, CV_64F);
    R.copyTo(m_privImpl->R);
}

const Mat & CameraGeometricParameters::t() const
{
    return m_privImpl->t;
}

void CameraGeometricParameters::setTranslationVector(const Mat &t)
{
    if ((t.rows != 3) || (t.cols != 1) || (t.depth() != CV_64F) || (t.channels() != 1))
    {
        logWarning() << "Given t is not a 3x1 64bit float vector, ignoring.";

        return;
    }
    m_privImpl->t.create(3, 1, CV_64F);
    t.copyTo(m_privImpl->t);
}

void CameraGeometricParameters::setExtrinsicMatrix(const Mat &Rt)
{
    if ((Rt.rows != 3) || (Rt.cols != 4) || (Rt.depth() != CV_64F) ||
        (Rt.channels() != 1))
    {
        logWarning() << "Given Rt is not a 3x4 64bit float vector, ignoring.";

        return;
    }
    m_privImpl->R.create(3, 3, CV_64F);
    m_privImpl->t.create(3, 1, CV_64F);
    Rt(Range(0, 3), Range(0, 3)).copyTo(m_privImpl->R);
    Rt(Range(0, 3), Range(3, 4)).copyTo(m_privImpl->t);
}

const Mat & CameraGeometricParameters::M() const
{
    static Mat M_;
    static Mat Rt(3, 4, CV_64F);
    m_privImpl->R.copyTo(Rt(Range(0, 3), Range(0, 3)));
    m_privImpl->t.copyTo(Rt(Range(0, 3), Range(3, 4)));
    M_ = m_privImpl->K * Rt;

    return M_;
}

void CameraGeometricParameters::setMatrix(const Mat &M)
{
    if ((M.rows != 3) || (M.cols != 4) || (M.depth() != CV_64F) || (M.channels() != 1))
    {
        logWarning() << "Given M is not a 3x4 64bit float vector, ignoring.";

        return;
    }
    m_privImpl->K.create(3, 3, CV_64F);
    m_privImpl->R.create(3, 3, CV_64F);
    m_privImpl->t.create(3, 1, CV_64F);
    decompositionKRT(M, m_privImpl->K, m_privImpl->R, m_privImpl->t);
}

Size CameraGeometricParameters::imageSize() const
{
    return m_privImpl->imageSize;
}

void CameraGeometricParameters::setImageSize(const Size &imageSize)
{
    m_privImpl->imageSize = imageSize;
}

double CameraGeometricParameters::k1()
{
    return (m_privImpl->coeffs.empty() ? numeric_limits<double>::quiet_NaN() :
                                         m_privImpl->coeffs.ptr<double>()[0]);
}

double CameraGeometricParameters::k2()
{
    return (m_privImpl->coeffs.empty() ? numeric_limits<double>::quiet_NaN() :
                                         m_privImpl->coeffs.ptr<double>()[1]);
}

double CameraGeometricParameters::p1()
{
    return (m_privImpl->coeffs.empty() ? numeric_limits<double>::quiet_NaN() :
                                         m_privImpl->coeffs.ptr<double>()[2]);
}

double CameraGeometricParameters::p2()
{
    return (m_privImpl->coeffs.empty() ? numeric_limits<double>::quiet_NaN() :
                                         m_privImpl->coeffs.ptr<double>()[3]);
}

const Mat & CameraGeometricParameters::distorsionCoefficients() const
{
    return m_privImpl->coeffs;
}

void CameraGeometricParameters::setDistorsionCoefficients(const cv::Mat &coeffs)
{
    m_privImpl->coeffs.create(4, 1, CV_64F);
    coeffs.copyTo(m_privImpl->coeffs);
}

bool CameraGeometricParameters::read(const string &path)
{
    FileStorage fs(path, FileStorage::READ);
    if (!fs.isOpened())
    {
        logWarning() << "Could not open camera file " << path;

        return false;
    }

    return read(fs);
}

bool CameraGeometricParameters::read(const FileStorage &fs)
{
    FileNode node;

    Mat M_;
    node = fs["M"];
    if (!node.empty()) { node >> M_; }

    Mat K_, R_, t_;
    node = fs["K"];
    if (!node.empty()) { node >> K_; }
    node = fs["R"];
    if (!node.empty()) { node >> R_; }
    node = fs["t"];
    if (!node.empty()) { node >> t_; }

    if (!M_.empty())
    {
        setMatrix(M_);

        if (norm(K_, K()) > 1E-4)
        {
            logWarning()
                << "Stored K is different from the one computed from M, ignoring K.";
        }
        if (norm(R_, R()) > 1E-4)
        {
            logWarning()
                << "Stored R is different from the one computed from M, ignoring R.";
        }
        if (norm(t_, t()) > 1E-4)
        {
            logWarning()
                << "Stored t is different from the one computed from M, ignoring t.";
        }
    }
    else
    {
        setIntrinsicMatrix(K_);
        setRotationMatrix(R_);
        setTranslationVector(t_);
    }

    Mat coeffs_;
    node = fs["coeffs"];
    if (!node.empty()) { node >> coeffs_; }
    setDistorsionCoefficients(coeffs_);

    Size imageSize_;
    node = fs["imageSize"];
    if (!node.empty()) { node >> imageSize_; }
    setImageSize(imageSize_);

    return true;
}

bool CameraGeometricParameters::write(const string &path)
{
    FileStorage fs(path, FileStorage::WRITE);
    if (!fs.isOpened())
    {
        logWarning() << "Could not open camera file" << path;

        return false;
    }

    return write(fs);
}

bool CameraGeometricParameters::write(FileStorage &fs)
{
    fs << "K" << K();
    fs << "R" << R();
    fs << "t" << t();
    // fs << "M" << M;
    fs << "coeffs" << distorsionCoefficients();
    fs << "imageSize" << imageSize();

    return true;
}

bool CameraGeometricParameters::empty() const
{
    return K().empty() && R().empty() && t().empty()
           && distorsionCoefficients().empty();
}

