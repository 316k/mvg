/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file is largely inspired by OpenCV's version of algorithms for
 * fundamental estimation
 */

#include <iostream>
#include <ceres/ceres.h>

#include "lsvd.h"
#include "homog.h"
#include "mvg.h"

using namespace std;
using namespace cv;
using namespace ceres;

struct HomographyEstimator::Private
{
    Private();
    ~Private();
    Mat T1, T2; // normalization matrices
};

HomographyEstimator::Private::Private()
{
    T1.create(3, 3, CV_64F);
    T2.create(3, 3, CV_64F);
}

HomographyEstimator::Private::~Private()
{ }

HomographyEstimator::HomographyEstimator(int modelPoints) :
    ModelEstimator(modelPoints, cv::Size(3, 3), 1)
{
    assert(modelPoints == 4);
    m_privImpl = new Private();
}

HomographyEstimator::~HomographyEstimator()
{
    delete m_privImpl;
}

/*virtual*/
void HomographyEstimator::initialize(const Mat &m1,
                                     const Mat &m2,
                                     Size       size1,
                                     Size       size2)
{
    if (size1 != Size())
    {
        double scalex1 = 2.0 / size1.width, scaley1 = 2.0 / size1.height;
        (Mat_<double>(3, 3, m_privImpl->T1.ptr<double>()) <<
            scalex1, 0, -1,
            0, scaley1, -1,
            0, 0, 1);
    }
    else
    {
        m_privImpl->T1 = normalizationMatrix(m1);
    }

    if (size2 != Size())
    {
        double scalex2 = 2.0 / size2.width, scaley2 = 2.0 / size2.height;
        (Mat_<double>(3, 3, m_privImpl->T2.ptr<double>()) <<
            scalex2, 0, -1,
            0, scaley2, -1,
            0, 0, 1);
    }
    else
    {
        m_privImpl->T2 = normalizationMatrix(m2);
    }
}

/*virtual*/
int HomographyEstimator::runLSKernel(const Mat &m1,
                                     const Mat &m2,
                                     Mat       &model)
{
    assert((m1.cols == 1 || m1.rows == 1) &&
           (m1.cols * m1.rows == m2.rows * m2.cols));

    const Point2d *p1    = m1.ptr<Point2d>(), *p2 = m2.ptr<Point2d>();
    int            count = m1.rows * m1.cols;
    Mat            A(3 * count, 9, CV_64F), Ai;

    // don't need to compute centers and average distances, they are given in
    // the preconditionning parameters corresponding to the size of the image
    // don't need to compute centers and average distances, they are given in
    // the preconditionning parameters corresponding to the size of the image
    double sx0 = m_privImpl->T1.at<double>(0, 0), ox0 = m_privImpl->T1.at<double>(0, 2),
           sy0 = m_privImpl->T1.at<double>(1, 1), oy0 = m_privImpl->T1.at<double>(1, 2);
    double sx1 = m_privImpl->T2.at<double>(0, 0), ox1 = m_privImpl->T2.at<double>(0, 2),
           sy1 = m_privImpl->T2.at<double>(1, 1), oy1 = m_privImpl->T2.at<double>(1, 2);

    A = Scalar::all(0);

    // form a linear system Ax=0: for each selected pair of points m1 & m2, the
    // row of A(=a) represents the coefficients of equation: (m2, 1)'*F*(m1, 1)
    for (int i = 0; i < count; i++)
    {
        double x0 = (p1[i].x * sx0) + ox0;
        double y0 = (p1[i].y * sy0) + oy0;

        double x1 = (p2[i].x * sx1) + ox1;
        double y1 = (p2[i].y * sy1) + oy1;

        double _r1[] = { 0, 0, 0, -x0, -y0, -1, x0 * y1, y0 * y1, y1 };
        Mat    r1(1, 9, CV_64F, _r1);
        Ai = A.row(i * 3);
        r1.copyTo(Ai);
        double _r2[] = { x0, y0, 1, 0, 0, 0, -x0 * x1, -y0 * x1, -x1 };
        Mat    r2(1, 9, CV_64F, _r2);
        Ai = A.row(i * 3 + 1);
        r2.copyTo(Ai);
        double _r3[] = { -x0 * y1, -y0 * y1, -y1, x0 * x1, y0 * x1, x1, 0, 0, 0 };
        Mat    r3(1, 9, CV_64F, _r3);
        Ai = A.row(i * 3 + 2);
        r3.copyTo(Ai);
    }

    /*
     * Eigen::MatrixXd mA;
     *   cv2eigen(A, mA);
     *   Eigen::MatrixXd mSvd;
     */
    // REDSVD::RedSVD svdOfA(mA);
    // cout << svdOfA.matrixV() << endl;
    LSVD lsvd(A, LSVD::MODIFY_A | LSVD::FULL_UV);

    // double *_w = svd.w.ptr<double>();
    double *_w = lsvd.w().ptr<double>();
    int     i;
    for (i = 0; i < 9; i++)
    {
        if (fabs(_w[i]) < numeric_limits<double>::epsilon())
        {
            break;
        }
    }
    if (i < 8) { return 0; }

    // take the last column of v as a solution of Af = 0
    // Mat H = Mat(3, 3, CV_64F, svd.vt.ptr<double>(8));
    Mat H = Mat(3, 3, CV_64F, lsvd.vt().ptr<double>(8));

    // apply the transformation that is inverse
    // to what we used to normalize the point coordinates
    model = m_privImpl->T2.inv() * H * m_privImpl->T1;
    // make H(3,3) = 1
    if (fabs(model.at<double>(2, 2)) > numeric_limits<double>::epsilon())
    {
        model /= model.at<double>(2, 2);
    }

    return 1;
}

#include <iostream>
using namespace std;

/*virtual*/
int HomographyEstimator::runKernel(const Mat &m1,
                                   const Mat &m2,
                                   Mat       &model)
{
    assert((m1.cols == 1 || m1.rows == 1) &&
           (m1.cols * m1.rows == m2.rows * m2.cols));

    const Point2d *p1 = m1.ptr<Point2d>(), *p2 = m2.ptr<Point2d>();
    Mat            A(2 * 4, 9, CV_64F), Ai;

    // don't need to compute centers and average distances, they are given in
    // the preconditionning parameters corresponding to the size of the image
    // don't need to compute centers and average distances, they are given in
    // the preconditionning parameters corresponding to the size of the image
    double scalex1 = m_privImpl->T1.at<double>(0, 0),
           scaley1 = m_privImpl->T1.at<double>(1, 1);
    double scalex2 = m_privImpl->T2.at<double>(0, 0),
           scaley2 = m_privImpl->T2.at<double>(1, 1);
    A = Scalar::all(0);

    // form a linear system Ax=0: for each selected pair of points m1 & m2, the
    // row of A(=a) represents the coefficients of equation: (m2, 1)'*F*(m1, 1)
    for (int i = 0; i < 4; i++)
    {
        double x0 = (p1[i].x * scalex1) - 1;
        double y0 = (p1[i].y * scaley1) - 1;
        double x1 = (p2[i].x * scalex2) - 1;
        double y1 = (p2[i].y * scaley2) - 1;

        double _r1[] = { 0, 0, 0, -x0, -y0, -1, x0 * y1, y0 * y1, y1 };
        Mat    r1(1, 9, CV_64F, _r1);
        Ai = A.row(i * 2);
        r1.copyTo(Ai);
        double _r2[] = { x0, y0, 1, 0, 0, 0, -x0 * x1, -y0 * x1, -x1 };
        Mat    r2(1, 9, CV_64F, _r2);
        Ai = A.row(i * 2 + 1);
        r2.copyTo(Ai);
    }

    LSVD lsvd(A, LSVD::MODIFY_A | LSVD::FULL_UV);

    double *_w = lsvd.w().ptr<double>();
    int     i;
    for (i = 0; i < 9; i++)
    {
        if (fabs(_w[i]) < numeric_limits<double>::epsilon())
        {
            break;
        }
    }
    if (i < 8) { return 0; }

    // take the last column of v as a solution of Af = 0
    // Mat H = Mat(3, 3, CV_64F, svd.vt.ptr<double>(8));
    Mat H = Mat(3, 3, CV_64F, lsvd.vt().ptr<double>(8));

    // apply the transformation that is inverse
    // to what we used to normalize the point coordinates
    model = m_privImpl->T2.inv() * H * m_privImpl->T1;
    // make H(3,3) = 1
    if (fabs(model.at<double>(2, 2)) > numeric_limits<double>::epsilon())
    {
        model /= model.at<double>(2, 2);
    }

    return 1;
}

/*virtual*/
void HomographyEstimator::computeReprojError(const Mat      &m1,
                                             const Mat      &m2,
                                             const Mat      &model,
                                             vector<double> &errors)
{
    size_t         count = static_cast<size_t>(m1.rows * m1.cols);
    const Point2d *p1    = m1.ptr<Point2d>(), *p2 = m2.ptr<Point2d>();
    const Mat     &H     = model;
    Mat            iH    = H.inv();

    for (size_t i = 0; i < count; i++)
    {
        errors[i] = max(squaredDistanceToHomographyProjection(H, p1[i], p2[i]),
                        squaredDistanceToHomographyProjection(iH, p2[i], p1[i]));
    }
}

int findHomographyMatrix(const Mat    &points1,
                         const Mat    &points2,
                         Mat          &hmatrix,
                         vector<bool> *mask,
                         int           method,
                         double        param1,
                         double        param2,
                         Size          size1,
                         Size          size2)
{
    int    result = 0;
    size_t count;

    double _H[3 * 3];
    Mat    H(3, 3, CV_64F, _H);

    Mat m1 = points1.clone(), m2 = points2.clone();
    assert(m1.rows * m1.cols == m2.rows * m2.cols);

    count = static_cast<size_t>(MAX(m1.cols, m1.rows));

    if (count < 4)
    {
        return 0;
    }

    vector<bool>        tempMask(count, true);
    HomographyEstimator estimator(4);

    estimator.initialize(m1, m2, size1, size2);

    if (count == 4)
    {
        result = estimator.runKernel(m1, m2, H);
    }
    else
    {
        if (param1 <= 0)
        {
            param1 = 3;
        }
        if ((param2 < DBL_EPSILON) || (param2 > 1 - DBL_EPSILON))
        {
            param2 = 0.99;
        }

        if (((method & ~3) == HM_LMEDS) && (count >= 2 * 4 + 1))
        {
            result = estimator.runLMeDS(m1, m2, H, tempMask, param2);
        }
        else
        {
            result = estimator.runRANSAC(m1, m2, H, tempMask, param1, param2);
        }

        if (result <= 0)
        {
            return 0;
        }
    }

    if (result) { H.copyTo(hmatrix); }

    if (mask)
    {
        mask->resize(count);
        copy(tempMask.begin(), tempMask.end(), mask->begin());
    }

    return result;
}

struct HomographyNonLinearEstimator
{
    HomographyNonLinearEstimator(double x0_,
                                 double y0_,
                                 double x1_,
                                 double y1_) :
        x0(x0_),
        y0(y0_),
        x1(x1_),
        y1(y1_)
    { }

    template <typename T> bool operator()(const T *const H,
                                          const T *const pe,
                                          T             *residuals) const
    {

        T p[2] = { T(x0), T(y0) };
        T q[2] = { T(x1), T(y1) };

        residuals[0] = ceres::sqrt(homographyGeometricDistance(H, pe, p, q));

        return true;
    }

    double x0, y0;
    double x1, y1;
};

/*virtual*/
bool HomographyEstimator::runOptimization(const Mat    &m1,
                                          const Mat    &m2,
                                          Mat          &model,
                                          vector<bool> &mask)
{
    size_t count = static_cast<size_t>(m1.rows * m1.cols);
    double sx0   = m_privImpl->T1.at<double>(0, 0), ox0 = m_privImpl->T1.at<double>(0, 2),
           sy0   = m_privImpl->T1.at<double>(1, 1), oy0 = m_privImpl->T1.at<double>(1, 2);
    double sx1   = m_privImpl->T2.at<double>(0, 0), ox1 = m_privImpl->T2.at<double>(0, 2),
           sy1   = m_privImpl->T2.at<double>(1, 1), oy1 = m_privImpl->T2.at<double>(1, 2);

    double paramsH[3 * 3];
    Mat    H(3, 3, CV_64F, paramsH);
    H = m_privImpl->T2 * model * m_privImpl->T1.inv();

    Mat mn1(static_cast<int>(count), 1, CV_64FC2);
    Mat mn2(static_cast<int>(count), 1, CV_64FC2);

    const Point2d *p1      = m1.ptr<Point2d>();
    const Point2d *p2      = m2.ptr<Point2d>();
    double        *paramsp = mn1.ptr<double>();
    Point2d       *pn2     = mn2.ptr<Point2d>();

    Problem problem;
    for (size_t i = 0; i < count; ++i)
    {
        if (mask[i])
        {
            paramsp[i * 2]     = (p1[i].x * sx0) + ox0;
            paramsp[i * 2 + 1] = (p1[i].y * sy0) + oy0;

            pn2[i].x = (p2[i].x * sx1) + ox1;
            pn2[i].y = (p2[i].y * sy1) + oy1;

            CostFunction *cost_function = new AutoDiffCostFunction<
                HomographyNonLinearEstimator, 1, 9, 2>(
                new HomographyNonLinearEstimator(
                    paramsp[i * 2], paramsp[i * 2 + 1],
                    pn2[i].x, pn2[i].y));

            /*
             *   Mat tX = (Mat_<double>(4,1) << paramsX[i*3+0],
             *        paramsX[i*3+1], paramsX[i*3+2], 1);
             *   Mat tp0 = M1*tX; tp0 /= tp0.at<double>(2,0);
             *   Mat tp1 = M2*tX; tp1 /= tp1.at<double>(2,0);
             *   Mat tp = (Mat_<double>(2,1) << pn1[i].x, pn1[i].y);
             *   Mat tq = (Mat_<double>(2,1) << pn2[i].x, pn2[i].y);
             *   cout << tX << tp0 << tp << tp1 << tq << endl;
             */

            problem.AddResidualBlock(cost_function, new HuberLoss(1),
                                     // paramsM, paramsE, paramsX+i*3);
                                     paramsH, paramsp + i * 2);
        }
    }

    Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_SCHUR;
    // options.ordering_type = ceres::SCHUR;
    // options.minimizer_progress_to_stdout = true;
    // options.num_linear_solver_threads = 2;
    // options.num_threads = 2;

    Solver::Summary summary;
    Solve(options, &problem, &summary);
    cout << summary.FullReport() << "\n";

    model = m_privImpl->T2.inv() * H * m_privImpl->T1;

    return true;
}

/* End of file. */

