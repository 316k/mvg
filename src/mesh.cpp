/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <stdio.h>
#include "mesh.h"

using namespace std;
using namespace cv;

struct Mesh::Private
{
    Private();
    ~Private();

    vector<Vec3u> triangles;
    vector<Vec2d> texCoords;
    vector<Vec3d> vertices;
    vector<Vec3d> normals;
    vector<Vec4b> colors;
};

Mesh::Private::Private() { }
Mesh::Private::~Private() { }

Mesh::Mesh()
{
    m_privImpl = new Private();
}

Mesh::Mesh(const Mesh &h) :
    m_privImpl(new Private(*h.m_privImpl))
{ }

Mesh & Mesh::operator=(const Mesh &h)
{
    if (this != &h)
    {
        delete m_privImpl;
        m_privImpl = new Private(*h.m_privImpl);
    }

    return *this;
}

Mesh::~Mesh()
{
    delete m_privImpl;
}

void Mesh::reverseNormals()
{
    size_t count = m_privImpl->normals.size();
    for (size_t i = 0; i < count; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            m_privImpl->normals[i][j] = -m_privImpl->normals[i][j];
        }
    }
}

void Mesh::clip(double xmin,
                double ymin,
                double zmin,
                double xmax,
                double ymax,
                double zmax)
{
    size_t      nverts = m_privImpl->vertices.size();
    vector<int> indices(nverts, -1);

    size_t j = 0;
    for (size_t i = 0; i < nverts; ++i)
    {
        if (!((m_privImpl->vertices[i][0] < xmin) ||
              (m_privImpl->vertices[i][0] > xmax) ||
              (m_privImpl->vertices[i][1] < ymin) ||
              (m_privImpl->vertices[i][1] > ymax) ||
              (m_privImpl->vertices[i][2] < zmin) ||
              (m_privImpl->vertices[i][2] > zmax)))
        {
            indices[i] = static_cast<int>(j);
            ++j;
        }
    }
    // nothing clipped
    if (nverts == j) { return; }

    remapIndices(indices);
}

void Mesh::removeUnusedVertices()
{
    size_t nverts = m_privImpl->vertices.size();
    if (nverts == 0) { return; }
    size_t ntriangles = m_privImpl->triangles.size();
    if (ntriangles == 0) { return; }

    vector<bool> used(nverts, false);
    for (size_t i = 0; i < ntriangles; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            used[m_privImpl->triangles[i][j]] = true;
        }
    }

    vector<int> indices(nverts, -1);
    size_t      j = 0;
    for (size_t i = 0; i < nverts; ++i)
    {
        if (used[i])
        {
            indices[i] = static_cast<int>(j);
            ++j;
        }
    }
    // nothing removed
    if (nverts == j) { return; }

    remapIndices(indices);
}

void Mesh::removeVertices(const set<int> &ids)
{
    // nothing removed
    if (ids.empty()) { return; }

    size_t                   nverts = m_privImpl->vertices.size();
    vector<int>              indices(nverts, -1);
    set<int>::const_iterator it = ids.begin();

    size_t j = 0;
    for (size_t i = 0; i < nverts; ++i)
    {
        if ((it == ids.end()) || (i != static_cast<size_t>(*it)))
        {
            indices[i] = static_cast<int>(j);
            ++j;
        }
        else
        {
            ++it;
        }
    }

    remapIndices(indices);
}

void Mesh::computeNormals(bool recompute)
{
    if ((m_privImpl->normals.size() > 0) && !recompute) { return; }

    size_t nverts     = m_privImpl->vertices.size();
    size_t ntriangles = m_privImpl->triangles.size();

    m_privImpl->normals.resize(nverts);
    cv::Vec3d u, v, n;
    for (size_t i = 0; i < ntriangles; ++i)
    {
        u = m_privImpl->vertices[m_privImpl->triangles[i][1]] -
            m_privImpl->vertices[m_privImpl->triangles[i][0]];
        v = m_privImpl->vertices[m_privImpl->triangles[i][2]] -
            m_privImpl->vertices[m_privImpl->triangles[i][0]];
        n  = u.cross(v);
        n /= norm(n);

        for (int j = 0; j < 3; ++j)
        {
            m_privImpl->normals[m_privImpl->triangles[i][j]] += n;
        }
    }

    for (size_t i = 0; i < nverts; ++i)
    {
        m_privImpl->normals[i] /= norm(m_privImpl->normals[i]);
    }
}

void Mesh::remapIndices(const std::vector<int> &indices)
{
    size_t nverts     = m_privImpl->vertices.size();
    size_t ntriangles = m_privImpl->triangles.size();
    size_t j          = 0;
    for (size_t i = 0; i < nverts; ++i)
    {
        if (indices[i] >= 0)
        {
            m_privImpl->vertices[static_cast<size_t>(indices[i])] =
                m_privImpl->vertices[i];
            ++j;
        }
    }
    m_privImpl->vertices.resize(j);

    if (m_privImpl->colors.size() > 0)
    {
        j = 0;
        for (size_t i = 0; i < nverts; ++i)
        {
            if (indices[i] >= 0)
            {
                m_privImpl->colors[static_cast<size_t>(indices[i])] =
                    m_privImpl->colors[i];
                ++j;
            }
        }
        m_privImpl->colors.resize(j);
    }

    if (m_privImpl->normals.size() > 0)
    {
        j = 0;
        for (size_t i = 0; i < nverts; ++i)
        {
            if (indices[i] >= 0)
            {
                m_privImpl->normals[static_cast<size_t>(indices[i])] =
                    m_privImpl->normals[i];
                ++j;
            }
        }
        m_privImpl->normals.resize(j);
    }

    if (m_privImpl->texCoords.size() > 0)
    {
        j = 0;
        for (size_t i = 0; i < nverts; ++i)
        {
            if (indices[i] >= 0)
            {
                m_privImpl->texCoords[static_cast<size_t>(indices[i])] =
                    m_privImpl->texCoords[i];
                ++j;
            }
        }
        m_privImpl->texCoords.resize(j);
    }

    j = 0;
    for (size_t i = 0; i < ntriangles; ++i)
    {
        Vec3u face;
        int   k;
        for (k = 0; k < 3; ++k)
        {
            size_t ind = static_cast<size_t>(m_privImpl->triangles[i][k]);
            if (indices[ind] < 0) { break; }
            face[k] = static_cast<unsigned int>(indices[ind]);
        }
        if (k == 3)
        {
            m_privImpl->triangles[j] = face;
            ++j;
        }
    }
    m_privImpl->triangles.resize(j);
}

bool Mesh::write(const string &path,
                 bool          ascii) const
{
    FILE *f = fopen(path.c_str(), "wb");
    if (!f)
    {
        cerr << "Unable to open " << path << endl;

        return false;
    }

    fprintf(f, "ply\n");
    if (ascii) { fprintf(f, "format ascii 1.0\n"); }
    else { fprintf(f, "format binary_little_endian 1.0\n"); }

#if defined _MSC_VER
    fprintf(f, "element vertex %Iu\n", m_privImpl->vertices.size());
#else
    fprintf(f, "element vertex %zu\n", m_privImpl->vertices.size());
#endif

    fprintf(f, "property float x\n");
    fprintf(f, "property float y\n");
    fprintf(f, "property float z\n");
    if (m_privImpl->normals.size() != 0)
    {
        fprintf(f, "property float nx\n");
        fprintf(f, "property float ny\n");
        fprintf(f, "property float nz\n");
    }
    if (m_privImpl->colors.size() != 0)
    {
        fprintf(f, "property uchar red\n");
        fprintf(f, "property uchar green\n");
        fprintf(f, "property uchar blue\n");
        fprintf(f, "property uchar alpha\n");
    }
    if (m_privImpl->texCoords.size() != 0)
    {
        fprintf(f, "property float u\n");
        fprintf(f, "property float v\n");
    }

    if (m_privImpl->triangles.size() != 0)
    {
#if defined _MSC_VER
        fprintf(f, "element face %Iu\n", m_privImpl->triangles.size());
#else
        fprintf(f, "element face %zu\n", m_privImpl->triangles.size());
#endif
        fprintf(f, "property list uchar int vertex_indices\n");
    }
    fprintf(f, "end_header\n");

    if (ascii) { writeAscii(f); }
    else { writeBinary(f); }

    fclose(f);

    return true;
}

bool Mesh::read(const string &path)
{
    FILE *f = fopen(path.c_str(), "rb");
    if (!f)
    {
        cerr << "Unable to open " << path << endl;

        return false;
    }

    char buf[1000];
    if (!fgets(buf, 1000, f)) { return false; }
    if (strncmp(buf, "ply", 3) != 0) { return false; }

    bool ascii;
    if (!fgets(buf, 1000, f)) { return false; }
    if (string(buf) == "format ascii 1.0\n") { ascii = true; }
    else if (string(buf) == "format binary_little_endian 1.0\n")
    {
        ascii = false;
    }
    else
    {
        cerr << "Unsupported ply format : only ascii and little_endian are supported : "
             << buf << endl;

        return false;
    }

    size_t nverts;
    if (!fgets(buf, 1000, f)) { return false; }

    while (strncmp(buf, "comment", 7) == 0)
    {
        if (!fgets(buf, 1000, f)) { return false; }
    }

    if (strncmp(buf, "element vertex", 14) != 0) { return false; }
#ifdef _MSC_VER
    sscanf(buf, "element vertex %Iu", &nverts);
#else
    sscanf(buf, "element vertex %zu", &nverts);
#endif

    const string props[] = {
        "property float x\n",
        "property float y\n",
        "property float z\n",
        "property float nx\n",
        "property float ny\n",
        "property float nz\n",
        "property uchar red\n",
        "property uchar green\n",
        "property uchar blue\n",
        "property uchar alpha\n",
        "property float u\n",
        "property float v\n"
    };

    size_t       nprops = sizeof(props) / sizeof(const string);
    vector<bool> hasProps(nprops, false);
    if (!fgets(buf, 1000, f)) { return false; }
    while (strncmp(buf, "property", 8) == 0)
    {
        bool found = false;
        for (size_t j = 0; j < nprops; ++j)
        {
            if (string(buf) == props[j])
            {
                hasProps[j] = true;
                found       = true;
                break;
            }
        }
        if (!found)
        {
            cerr << "Unsupported property : " << buf << endl;

            return false;
        }
        if (!fgets(buf, 1000, f)) { return false; }
    }

    bool hasVertices  = hasProps[0] && hasProps[1] && hasProps[2];
    bool hasNormals   = hasProps[3] && hasProps[4] && hasProps[5];
    bool hasColors    = hasProps[6] && hasProps[7] && hasProps[8] && hasProps[9];
    bool hasTexCoords = hasProps[10] && hasProps[11];

    if (!hasVertices)
    {
        cerr << "No vertices found in PLY file" << endl;

        return false;
    }
    else { m_privImpl->vertices.resize(nverts); }
    if (hasNormals) { m_privImpl->normals.resize(nverts); }
    if (hasColors) { m_privImpl->colors.resize(nverts); }
    if (hasTexCoords) { m_privImpl->texCoords.resize(nverts); }

    if (strncmp(buf, "element face", 12) == 0)
    {
        size_t ntriangles;
#ifdef _MSC_VER
        sscanf(buf, "element face %Iu", &ntriangles);
#else
        sscanf(buf, "element face %zu", &ntriangles);
#endif
        m_privImpl->triangles.resize(ntriangles);

        if (!fgets(buf, 1000, f)) { return false; }
        if (string(buf) != "property list uchar int vertex_indices\n")
        {
            cerr << "Unsupported property : " << buf << endl;

            return false;
        }
        if (!fgets(buf, 1000, f)) { return false; }
    }

    if (string(buf) != "end_header\n")
    {
        cerr << "Unsupported element or property : " << buf << endl;

        return false;
    }

    if (ascii) { readAscii(f); }
    else { readBinary(f); }

    fclose(f);

    return true;
}

bool Mesh::writeAscii(FILE *f) const
{
    bool hasNormals   = m_privImpl->normals.size() > 0;
    bool hasColors    = m_privImpl->colors.size() > 0;
    bool hasTexCoords = m_privImpl->texCoords.size() > 0;

    for (size_t i = 0; i < m_privImpl->vertices.size(); ++i)
    {
        if (fprintf(f, "%g %g %g ", m_privImpl->vertices[i][0],
                    m_privImpl->vertices[i][1], m_privImpl->vertices[i][2]) < 0)
        {
            return false;
        }
        if (hasNormals)
        {
            if (fprintf(f, "%g %g %g ", m_privImpl->normals[i][0],
                        m_privImpl->normals[i][1], m_privImpl->normals[i][2]) < 0)
            {
                return false;
            }
        }
        if (hasColors)
        {
            if (fprintf(f, "%u %u %u %u ", m_privImpl->colors[i][0],
                        m_privImpl->colors[i][1], m_privImpl->colors[i][2],
                        m_privImpl->colors[i][3]) < 0)
            {
                return false;
            }
        }
        if (hasTexCoords)
        {
            if (fprintf(f, "%g %g ", m_privImpl->texCoords[i][0],
                        m_privImpl->texCoords[i][1]) < 0)
            {
                return false;
            }
        }
        if (fprintf(f, "\n") < 0) { return false; }
    }

    for (size_t i = 0; i < m_privImpl->triangles.size(); ++i)
    {
        if (fprintf(f, "3 %u %u %u\n", m_privImpl->triangles[i][0],
                    m_privImpl->triangles[i][1], m_privImpl->triangles[i][2]) < 0)
        {
            return false;
        }
    }

    return true;
}

bool Mesh::readAscii(FILE *f)
{
    bool hasNormals   = m_privImpl->normals.size() > 0;
    bool hasColors    = m_privImpl->colors.size() > 0;
    bool hasTexCoords = m_privImpl->texCoords.size() > 0;

    for (size_t i = 0; i < m_privImpl->vertices.size(); ++i)
    {
        if (fscanf(f, "%lg %lg %lg", &m_privImpl->vertices[i][0],
                   &m_privImpl->vertices[i][1], &m_privImpl->vertices[i][2]) != 3)
        {
            return false;
        }
        if (hasNormals)
        {
            if (fscanf(f, "%lg %lg %lg", &m_privImpl->normals[i][0],
                       &m_privImpl->normals[i][1], &m_privImpl->normals[i][2]) != 3)
            {
                return false;
            }
        }
        if (hasColors)
        {
            if (fscanf(f, "%hhu %hhu %hhu %hhu", &m_privImpl->colors[i][0],
                       &m_privImpl->colors[i][1], &m_privImpl->colors[i][2],
                       &m_privImpl->colors[i][3]) != 4)
            {
                return false;
            }
        }
        if (hasTexCoords)
        {
            if (fscanf(f, "%lg %lg ", &m_privImpl->texCoords[i][0],
                       &m_privImpl->texCoords[i][1]) != 2)
            {
                return false;
            }
        }
    }

    for (size_t i = 0; i < m_privImpl->triangles.size(); ++i)
    {
        int tmp;
        if ((fscanf(f, "%d ", &tmp) != 1) || (tmp != 3)) { return false; }
        if (fscanf(f, "%u %u %u", &m_privImpl->triangles[i][0],
                   &m_privImpl->triangles[i][1], &m_privImpl->triangles[i][2]))
        {
            return false;
        }
    }

    return true;
}

bool byte_write(FILE         *f,
                const double *p,
                size_t        n);
bool byte_write(FILE        *f,
                const float *p,
                size_t       n);
bool byte_write(FILE        *f,
                const uchar *p,
                size_t       n);
bool byte_write(FILE               *f,
                const unsigned int *p,
                size_t              n);

bool Mesh::writeBinary(FILE *f) const
{
    bool hasNormals   = m_privImpl->normals.size() > 0;
    bool hasColors    = m_privImpl->colors.size() > 0;
    bool hasTexCoords = m_privImpl->texCoords.size() > 0;

    for (size_t i = 0; i < m_privImpl->vertices.size(); ++i)
    {
        byte_write(f, reinterpret_cast<const double *>(&m_privImpl->vertices[i]), 3);
        if (hasNormals)
        {
            byte_write(f, reinterpret_cast<const double *>(&m_privImpl->normals[i]),
                       3);
        }
        if (hasColors)
        {
            byte_write(f, reinterpret_cast<const uchar *>(&m_privImpl->colors[i]),
                       4);
        }
        if (hasTexCoords)
        {
            byte_write(f, reinterpret_cast<const double *>(&m_privImpl->texCoords[i]),
                       2);
        }
    }

    uchar tmp = 3;
    for (size_t i = 0; i < m_privImpl->triangles.size(); ++i)
    {
        byte_write(f, &tmp, 1);
        byte_write(f, reinterpret_cast<const unsigned int *>(&m_privImpl->triangles[i]),
                   3);
    }

    return true;
}

bool byte_read(FILE   *f,
               double *p,
               size_t  n);
bool byte_read(FILE  *f,
               float *p,
               size_t n);
bool byte_read(FILE  *f,
               uchar *p,
               size_t n);
bool byte_read(FILE         *f,
               unsigned int *p,
               size_t        n);

bool Mesh::readBinary(FILE *f)
{
    bool hasNormals   = m_privImpl->normals.size() > 0;
    bool hasColors    = m_privImpl->colors.size() > 0;
    bool hasTexCoords = m_privImpl->texCoords.size() > 0;

    for (size_t i = 0; i < m_privImpl->vertices.size(); ++i)
    {
        byte_read(f, reinterpret_cast<double *>(&m_privImpl->vertices[i]), 3);
        if (hasNormals)
        {
            byte_read(f, reinterpret_cast<double *>(&m_privImpl->normals[i]),
                      3);
        }
        if (hasColors)
        {
            byte_read(f, reinterpret_cast<uchar *>(&m_privImpl->colors[i]),
                      4);
        }
        if (hasTexCoords)
        {
            byte_read(f, reinterpret_cast<double *>(&m_privImpl->texCoords[i]),
                      2);
        }
    }

    uchar tmp;
    for (size_t i = 0; i < m_privImpl->triangles.size(); ++i)
    {
        byte_read(f, &tmp, 1);
        byte_read(f, reinterpret_cast<unsigned int *>(&m_privImpl->triangles[i]), 3);
    }

    return true;
}

inline unsigned int byte_swap(const unsigned int &val)
{
    return (((val) & 0xff000000) >> 24) |
           (((val) & 0x00ff0000) >>  8) |
           (((val) & 0x0000ff00) <<  8) |
           (((val) & 0x000000ff) << 24);
}

inline float byte_swap(const double &val)
{
    union
    {
        float        v1;
        unsigned int v2;
    }
    a, b;
    a.v1 = static_cast<float>(val);
    b.v2 = byte_swap(a.v2);

    return b.v1;
}

inline double byte_swap(const float &val)
{
    union
    {
        float        v1;
        unsigned int v2;
    }
    a, b;
    a.v1 = val;
    b.v2 = byte_swap(a.v2);

    return b.v1;
}

inline bool byte_write(FILE         *f,
                       const double *p,
                       size_t        n)
{
    static vector<float> buf;
    buf.resize(n);

#ifdef BIGENDIAN
    for (size_t i = 0; i < n; ++i)
    {
        buf[i] = byte_swap(p[i]);
    }
#else
    for (size_t i = 0; i < n; ++i)
    {
        buf[i] = static_cast<float>(p[i]);
    }
#endif

    return fwrite(buf.data(), sizeof(float), n, f) == n;
}

inline bool byte_write(FILE        *f,
                       const float *p,
                       size_t       n)
{
#ifdef BIGENDIAN
    static vector<float> buf;
    buf.resize(n);
    for (int i = 0; i < n; ++i)
    {
        buf[i] = byte_swap(p[i]);
    }

    return fwrite(buf.data(), sizeof(float), n, f) == n;

#else

    return fwrite(p, sizeof(float), n, f) == n;

#endif
}

inline bool byte_write(FILE               *f,
                       const unsigned int *p,
                       size_t              n)
{
#ifdef BIGENDIAN
    static vector<unsigned int> buf;
    buf.resize(n);
    for (size_t i = 0; i < n; ++i)
    {
        buf[i] = byte_swap(p[i]);
    }

    return fwrite(buf.data(), sizeof(unsigned int), n, f) == n;

#else

    return fwrite(p, sizeof(unsigned int), n, f) == n;

#endif
}

inline bool byte_write(FILE        *f,
                       const uchar *p,
                       size_t       n)
{
    return fwrite(p, sizeof(uchar), n, f) == n;
}

inline bool byte_read(FILE   *f,
                      double *p,
                      size_t  n)
{
    static vector<float> buf;
    buf.resize(n);
    if (fread(buf.data(), sizeof(float), n, f) != n) { return false; }

#ifdef BIGENDIAN
    for (size_t i = 0; i < n; ++i)
    {
        p[i] = byte_swap(buf[i]);
    }
#else
    copy(buf.begin(), buf.end(), p);
#endif

    return true;
}

inline bool byte_read(FILE  *f,
                      float *p,
                      size_t n)
{
#ifdef BIGENDIAN
    static vector<float> buf;
    buf.resize(n);
    if (fread(buf.data(), sizeof(float), n, f) != n) { return false; }

    for (size_t i = 0; i < n; ++i)
    {
        p[i] = byte_swap(buf[i]);
    }

    return true;

#else

    return fread(p, sizeof(float), n, f) == n;

#endif
}

inline bool byte_read(FILE         *f,
                      unsigned int *p,
                      size_t        n)
{
#ifdef BIGENDIAN
    static vector<unsigned int> buf;
    buf.resize(n);
    if (fread(buf.data(), sizeof(unsigned int), n, f) != n) { return false; }

    for (size_t i = 0; i < n; ++i)
    {
        p[i] = byte_swap(buf[i]);
    }

    return true;

#else

    return fread(p, sizeof(unsigned int), n, f) == n;

#endif
}

inline bool byte_read(FILE  *f,
                      uchar *p,
                      size_t n)
{
    return fread(p, sizeof(uchar), n, f) == n;
}

vector<Mesh::Vec3u> &Mesh::triangles()
{
    return m_privImpl->triangles;
}

const vector<Mesh::Vec3u> &Mesh::triangles() const
{
    return m_privImpl->triangles;
}

vector<Vec2d> &Mesh::texCoords()
{
    return m_privImpl->texCoords;
}

const vector<Vec2d> &Mesh::texCoords() const
{
    return m_privImpl->texCoords;
}

vector<Vec3d> &Mesh::vertices()
{
    return m_privImpl->vertices;
}

const vector<Vec3d> &Mesh::vertices() const
{
    return m_privImpl->vertices;
}

vector<Vec3d> &Mesh::normals()
{
    return m_privImpl->normals;
}

const vector<Vec3d> &Mesh::normals() const
{
    return m_privImpl->normals;
}

vector<Vec4b> &Mesh::colors()
{
    return m_privImpl->colors;
}

const vector<Vec4b> &Mesh::colors() const
{
    return m_privImpl->colors;
}
