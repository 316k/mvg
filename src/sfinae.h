/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SFINAE_H
#define SFINAE_H

// Inspired by Boost

template <bool B, class T = void>
struct EnableIfCond
{
    typedef T type;
};

template <class T>
struct EnableIfCond<false, T> { };

template <class Cond, class T = void>
struct EnableIf : public EnableIfCond<Cond::value, T> { };

template <typename T, typename U>
struct IsSame
{
    static const bool value = false;
};

template <typename T>
struct IsSame<T, T>
{
    static const bool value = true;
};

namespace mvg
{
    namespace internal
    {
        typedef char ( &yes)[1];
        typedef char ( &no)[2];

        template <typename B, typename D>
        struct Host
        {
            operator B *() const;
            operator D *();
        };
    }
}

template <typename B, typename D>
struct IsBaseOf
{
    template <typename T> static mvg::internal::yes check(D *, T);
    static mvg::internal::no                        check(B *,
                                                          int);

    static const bool value =
        IsSame<B, D>::value ||
        (sizeof(check(mvg::internal::Host<B, D>(), int())) == sizeof(mvg::internal::yes));
};

#endif // SFINAE_H
