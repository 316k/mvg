/* CLAPACK 3.0 BLAS wrapper macros and functions
 * Feb 5, 2000
 */


#ifndef CBLAS_H
#define CBLAS_H

#ifndef NORETURN
#  if defined(__GNUC__)
#    define NORETURN __attribute__((__noreturn__))
#  elif defined(_MSC_VER) && (_MSC_VER >= 1300)
#    define NORETURN __declspec(noreturn)
#  else
#    define NORETURN /* nothing by default */
#  endif
#endif

#include "f2c.h"

#if defined _MSC_VER && _MSC_VER >= 1400
#pragma warning(disable: 4244 4554)
#endif

#ifdef __cplusplus
extern "C" {
#endif

static __inline double r_lg10(real *x)
{
    return 0.43429448190325182765*log(*x);
}

static __inline double d_lg10(doublereal *x)
{
    return 0.43429448190325182765*log(*x);
}

static __inline double d_sign(doublereal *a, doublereal *b)
{
    double x = fabs(*a);
    return *b >= 0 ? x : -x;
}

static __inline double r_sign(real *a, real *b)
{
#ifdef __cplusplus
    double x = fabs(static_cast<double>(*a));
#else
    double x = fabs((double)*a);
#endif
    return *b >= 0 ? x : -x;
}

extern const unsigned char lapack_toupper_tab[];
#ifdef __cplusplus
#define lapack_toupper(c) (static_cast<char>(lapack_toupper_tab[static_cast<unsigned char>(c)]))
#else
#define lapack_toupper(c) ((char)lapack_toupper_tab[(unsigned char)(c)])
#endif

extern const unsigned char lapack_lamch_tab[];
extern const doublereal lapack_dlamch_tab[];
extern const doublereal lapack_slamch_tab[];
    
static __inline logical lsame_(char *ca, char *cb)
{
    return lapack_toupper(ca[0]) == lapack_toupper(cb[0]);
}

static __inline doublereal dlamch_(char* cmach)
{
#ifdef __cplusplus
    return lapack_dlamch_tab[lapack_lamch_tab[static_cast<unsigned char>(cmach[0])]];
#else
    return lapack_dlamch_tab[lapack_lamch_tab[(unsigned char)cmach[0]]];
#endif
}
    
static __inline doublereal slamch_(char* cmach)
{
#ifdef __cplusplus
    return lapack_slamch_tab[lapack_lamch_tab[static_cast<unsigned char>(cmach[0])]];
#else
    return lapack_slamch_tab[lapack_lamch_tab[(unsigned char)cmach[0]]];
#endif
}    
    
static __inline integer i_nint(real *x)
{
#ifdef __cplusplus
    return static_cast<integer>(*x >= 0 ? floor(*x + .5) : -floor(.5 - *x));
#else
    return (integer)(*x >= 0 ? floor(*x + .5) : -floor(.5 - *x));
#endif
}

static NORETURN __inline void exit_(integer *rc)
{
    exit(*rc);
}

integer pow_ii(integer *ap, integer *bp);
double pow_ri(real *ap, integer *bp);
double pow_di(doublereal *ap, integer *bp);

static __inline double pow_dd(doublereal *ap, doublereal *bp)
{
    return pow(*ap, *bp);
}

logical slaisnan_(real *in1, real *in2);
logical dlaisnan_(doublereal *din1, doublereal *din2);

static __inline logical sisnan_(real *in1)
{
    return slaisnan_(in1, in1);
}

static __inline logical disnan_(doublereal *din1)
{
    return dlaisnan_(din1, din1);
}

char *F77_aloc(ftnlen, char*);

#ifdef __cplusplus
}
#endif
#undef NORETURN

#endif /* CBLAS_H */
