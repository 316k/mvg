/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

// *INDENT-OFF*

#include <OpenThreads/ScopedLock>
#include <errno.h>

template <class T, class Container, class Adapter>
struct AdapterIsNotFull
{
    AdapterIsNotFull(ConcurrentAdapter<T, Container, Adapter> *ad) : a(ad) { }
    bool operator()() const
    {
        return !(a->m_capacity > 0 &&
                 a->m_queue.size() >= a->m_capacity);
    }
    ConcurrentAdapter<T, Container, Adapter> *a;
};

template <class T, class Container, class Adapter>
struct AdapterIsNotEmpty
{
    AdapterIsNotEmpty(ConcurrentAdapter<T, Container, Adapter> *ad) : a(ad) { }
    bool operator()() const { return !(a->m_queue.empty()); }
    ConcurrentAdapter<T, Container, Adapter> *a;
};

template <class Adapter> struct FrontGetter { };

template <class T, class Container> struct FrontGetter< std::queue<T, Container> >
{
    typedef typename Container::value_type value_type;

    FrontGetter(std::queue<T, Container> &qu) : q(qu) { }
    value_type front() { return q.front(); }
    std::queue<T, Container> &q;
};

template <class T, class Container> struct FrontGetter< std::stack<T, Container> >
{
    typedef typename Container::value_type value_type;

    FrontGetter(std::stack<T, Container> &st) : s(st) { }
    value_type front() { return s.top(); }
    std::stack<T, Container> &s;
};

template <class T, class Container, class Adapter>
ConcurrentAdapter<T, Container, Adapter>::ConcurrentAdapter() : m_capacity(0)
{ }

template <class T, class Container, class Adapter>
ConcurrentAdapter<T, Container, Adapter>::ConcurrentAdapter(
    const ConcurrentAdapter<T, Container, Adapter> &sq)
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(sq.m_mutex);
    m_queue = sq.m_queue;
}

template <class T, class Container, class Adapter>
ConcurrentAdapter<T, Container, Adapter>::~ConcurrentAdapter()
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);
}

template <class T, class Container, class Adapter>
ConcurrentAdapter<T, Container, Adapter> &
ConcurrentAdapter<T, Container, Adapter>::operator=(
     const ConcurrentAdapter<T, Container, Adapter> &sq)
{
    if (this != &sq)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> lock1(m_mutex);
        OpenThreads::ScopedLock<OpenThreads::Mutex> lock2(sq.m_mutex);
        Adapter                                     temp(sq.m_queue);
        std::swap(m_queue, temp);

        if (AdapterIsNotFull<T, Container, Adapter>(this)())
        {
            m_conditionFull.broadcast();
        }
        if (AdapterIsNotEmpty<T, Container, Adapter>(this)())
        {
            m_conditionEmpty.broadcast();
        }
    }

    return *this;
}

template <class T, class Container, class Adapter>
unsigned int ConcurrentAdapter<T, Container, Adapter>::capacity() const
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    return m_capacity;
}

template <class T, class Container, class Adapter>
void ConcurrentAdapter<T, Container, Adapter>::set_capacity(unsigned int capacity)
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);
    m_capacity = capacity;
}

template <class Pred>
void wait(OpenThreads::Condition &condition,
          OpenThreads::Mutex     &mutex,
          const Pred             &pred)
{
    while (!pred())
    {
        condition.wait(&mutex);
    }
}

template <class Pred>
bool wait(OpenThreads::Condition &condition,
          OpenThreads::Mutex     &mutex,
          const Pred             &pred,
          unsigned long           ms)
{
    while (!pred())
    {
        int ret = condition.wait(&mutex, ms);
        if (ret == 0)
        {
            return pred();
        }
        else
        {
            return false;
        }
    }

    return true;
}

template <class T, class Container, class Adapter>
void ConcurrentAdapter<T, Container, Adapter>::push(
    const typename Container::value_type &item)
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    wait(m_conditionFull, m_mutex, AdapterIsNotFull<T, Container, Adapter>(this));

    m_queue.push(item);
    m_conditionEmpty.signal();
}

template <class T, class Container, class Adapter>
bool ConcurrentAdapter<T, Container, Adapter>::try_push(
    const typename Container::value_type &item)
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    if (!AdapterIsNotFull<T, Container, Adapter>(this)())
    {
        return false;
    }

    m_queue.push(item);
    m_conditionEmpty.signal();

    return true;
}

template <class T, class Container, class Adapter>
bool ConcurrentAdapter<T, Container, Adapter>::timeout_push(
    const typename Container::value_type &item,
    unsigned long int                     timeout)
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    if (!AdapterIsNotFull<T, Container, Adapter>(this)())
    {
        if (timeout == 0)
        {
            return false;
        }

        if (!wait(m_conditionEmpty, m_mutex,
                  AdapterIsNotFull<T, Container, Adapter>(this),
                  static_cast<unsigned long>(timeout / 1000.)))
        {
            return false;
        }

    }

    m_queue.push(item);
    m_conditionEmpty.signal();

    return true;
}

template <class T, class Container, class Adapter>
void ConcurrentAdapter<T, Container, Adapter>::pop(typename Container::value_type &item)
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    wait(m_conditionEmpty, m_mutex, AdapterIsNotEmpty<T, Container, Adapter>(this));

    FrontGetter<Adapter> getter(m_queue);
    item = getter.front();

    m_queue.pop();
    m_conditionFull.signal();
}

template <class T, class Container, class Adapter>
bool ConcurrentAdapter<T, Container, Adapter>::try_pop(
    typename Container::value_type &item)
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    if (!AdapterIsNotEmpty<T, Container, Adapter>(this)())
    {
        return false;
    }

    FrontGetter<Adapter> getter(m_queue);
    item = getter.front();

    m_queue.pop();
    m_conditionFull.signal();

    return true;
}

template <class T, class Container, class Adapter>
bool ConcurrentAdapter<T, Container, Adapter>::timeout_pop(
    typename Container::value_type &item,
    unsigned long int               timeout)
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    if (!AdapterIsNotEmpty<T, Container, Adapter>(this)())
    {
        if (timeout == 0)
        {
            return false;
        }

        if (!wait(m_conditionEmpty, m_mutex,
                  AdapterIsNotEmpty<T, Container, Adapter>(this),
                  static_cast<unsigned long>(timeout / 1000.)))
        {
            return false;
        }
    }

    FrontGetter<Adapter> getter(m_queue);
    item = getter.front();

    m_queue.pop();
    m_conditionFull.signal();

    return true;
}

template <class T, class Container, class Adapter>
typename Container::size_type ConcurrentAdapter<T, Container, Adapter>::size() const
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    return m_queue.size();
}

template <class T, class Container, class Adapter>
void ConcurrentAdapter<T, Container, Adapter>::clear()
{
    ConcurrentAdapter().swap(*this);
}

template <class T, class Container, class Adapter>
bool ConcurrentAdapter<T, Container, Adapter>::empty() const
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    return m_queue.empty();
}

template <class T, class Container, class Adapter>
bool ConcurrentAdapter<T, Container, Adapter>::full() const
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);

    return m_capacity > 0 && size() >= m_capacity;
}

template <class T, class Container, class Adapter>
void ConcurrentAdapter<T, Container, Adapter>::swap(
     ConcurrentAdapter<T, Container, Adapter> &sq)
{
    if (this != &sq)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> lock1(m_mutex);
        OpenThreads::ScopedLock<OpenThreads::Mutex> lock2(sq.m_mutex);
        std::swap(m_queue, sq.m_queue);

        if (AdapterIsNotFull<T, Container, Adapter>(this)())
        {
            m_conditionFull.broadcast();
        }
        if (AdapterIsNotEmpty<T, Container, Adapter>(this)())
        {
            m_conditionEmpty.broadcast();
        }

        if (AdapterIsNotFull<T, Container, Adapter>(&sq) ())
        {
            sq.m_conditionFull.broadcast();
        }
        if (AdapterIsNotEmpty<T, Container, Adapter>(&sq) ())
        {
            sq.m_conditionEmpty.broadcast();
        }
    }
}

template <class T, class Container>
void swap(ConcurrentAdapter<T, Container> &q1,
          ConcurrentAdapter<T, Container> &q2)
{
    q1.swap(q2);
}
// *INDENT-ON*
