/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIMILARITY_H_
#define SIMILARITY_H_

#include "modelest.h"
#include "export.h"

enum { ST_RANSAC, ST_LMEDS };

MVG_DECLSPEC int findSimilarityTransform(const cv::Mat     &points1,
                                         const cv::Mat     &points2,
                                         cv::Mat           &mmatrix,
                                         std::vector<bool> *mask=0,
                                         int                method=ST_RANSAC,
                                         double             param1=3.,
                                         double             param2=0.99,
                                         cv::Size           size=cv::Size());

MVG_DECLSPEC int findRigidTransform(const cv::Mat     &points1,
                                    const cv::Mat     &points2,
                                    cv::Mat           &mmatrix,
                                    std::vector<bool> *mask=0,
                                    int                method=ST_RANSAC,
                                    double             param1=3.,
                                    double             param2=0.99,
                                    cv::Size           size=cv::Size());

class MVG_DECLSPEC SimilarityEstimator : public ModelEstimator
{
    public:
        SimilarityEstimator(int  modelPoints,
                            bool rigid=false);
        ~SimilarityEstimator();

        void initialize(const cv::Mat &m2,
                        cv::Size       size);
        void initialize(const cv::Mat &m1,
                        const cv::Mat &m2);

        virtual int runLSKernel(const cv::Mat &m1,
                                const cv::Mat &m2,
                                cv::Mat       &model);

        virtual int runKernel(const cv::Mat &m1,
                              const cv::Mat &m2,
                              cv::Mat       &model);

        virtual bool runOptimization(const cv::Mat     &m1,
                                     const cv::Mat     &m2,
                                     cv::Mat           &model,
                                     std::vector<bool> &mask);

    protected:
        virtual void computeReprojError(const cv::Mat       &m1,
                                        const cv::Mat       &m2,
                                        const cv::Mat       &model,
                                        std::vector<double> &errors);

        virtual bool getSubset(const cv::Mat &m1,
                               const cv::Mat &m2,
                               cv::Mat       &ms1,
                               cv::Mat       &ms2,
                               int            maxAttempts=1000);

        virtual bool check3DSubset(const cv::Mat &ms,
                                   int            count);

        virtual int compressPoints(cv::Mat                 &m1,
                                   cv::Mat                 &m2,
                                   const std::vector<bool> &mask);

    private:
        struct Private;
        Private *m_privImpl;
};

#endif // SIMILARITY_H_

