/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Reimplementation of PCL's OrganizedFastMesher to support custom filtering
 */

#include "organized_fast_mesher.h"

/////////////////////////////////////////////////////////////////////////////////////////////
template<class Filter>
void OrganizedFastMesher<Filter>::reconstruct(cv::Ptr<Mesh>          &mesh,
                                              const cv::Size         &gridSize,
                                              const std::vector<int> &indices)
{
    if (triangulation_type_ == TRIANGLE_RIGHT_CUT)
    {
        makeRightCutMesh(mesh, gridSize, indices);
    }
    else if (triangulation_type_ == TRIANGLE_LEFT_CUT)
    {
        makeLeftCutMesh(mesh, gridSize, indices);
    }
    else if (triangulation_type_ == TRIANGLE_ADAPTIVE_CUT)
    {
        makeAdaptiveCutMesh(mesh, gridSize, indices);
    }
    mesh->removeUnusedVertices();
}

/////////////////////////////////////////////////////////////////////////////////////////////
template<class Filter>
void OrganizedFastMesher<Filter>::makeRightCutMesh(cv::Ptr<Mesh>          &mesh,
                                                   const cv::Size         &gridSize,
                                                   const std::vector<int> &indices)
{
    size_t width  = static_cast<size_t>(gridSize.width);
    size_t height = static_cast<size_t>(gridSize.height);

    size_t last_column = width - triangle_pixel_size_columns_;
    size_t last_row    = height - triangle_pixel_size_rows_;

    size_t i          = 0, index_down = 0, index_right = 0, index_down_right = 0, idx = 0;
    size_t y_big_incr = triangle_pixel_size_rows_ * width,
           x_big_incr = y_big_incr + triangle_pixel_size_columns_;

    // Reserve enough space
    mesh->triangles().resize(static_cast<size_t>(width * height * 2));

    // Go over the rows first
    for (size_t y = 0; y < last_row; y += triangle_pixel_size_rows_)
    {
        // Initialize a new row
        i                = y * width;
        index_right      = i + triangle_pixel_size_columns_;
        index_down       = i + y_big_incr;
        index_down_right = i + x_big_incr;

        // Go over the columns
        for (size_t x = 0; x < last_column; x += triangle_pixel_size_columns_,
             i += triangle_pixel_size_columns_,
             index_right += triangle_pixel_size_columns_,
             index_down += triangle_pixel_size_columns_,
             index_down_right += triangle_pixel_size_columns_)
        {
            int a = indices[i];
            int d = indices[index_down_right];
            int b = indices[index_right];
            int c = indices[index_down];

            if (isValidTriangle(a, d, b))
            {
                if (!isFilteredTriangle(a, d, b, mesh))
                {
                    addTriangle(a, d, b, idx++, mesh);
                }
            }

            if (isValidTriangle(a, c, d))
            {
                if (!isFilteredTriangle(a, c, d, mesh))
                {
                    addTriangle(a, c, d, idx++, mesh);
                }
            }
        }
    }
    mesh->triangles().resize(idx);
}

/////////////////////////////////////////////////////////////////////////////////////////////
template <class Filter>
void OrganizedFastMesher<Filter>::makeLeftCutMesh(cv::Ptr<Mesh>          &mesh,
                                                  const cv::Size         &gridSize,
                                                  const std::vector<int> &indices)
{
    size_t width  = static_cast<size_t>(gridSize.width);
    size_t height = static_cast<size_t>(gridSize.height);

    size_t last_column = width - triangle_pixel_size_columns_;
    size_t last_row    = height - triangle_pixel_size_rows_;

    size_t i          = 0, index_down = 0, index_right = 0, index_down_right = 0, idx = 0;
    size_t y_big_incr = triangle_pixel_size_rows_ * width,
           x_big_incr = y_big_incr + triangle_pixel_size_columns_;

    // Reserve enough space
    mesh->triangles().resize(width * height * 2);

    // Go over the rows first
    for (size_t y = 0; y < last_row; y += triangle_pixel_size_rows_)
    {
        // Initialize a new row
        i                = y * width;
        index_right      = i + triangle_pixel_size_columns_;
        index_down       = i + y_big_incr;
        index_down_right = i + x_big_incr;

        // Go over the columns
        for (size_t x = 0; x < last_column; x += triangle_pixel_size_columns_,
             i += triangle_pixel_size_columns_,
             index_right += triangle_pixel_size_columns_,
             index_down += triangle_pixel_size_columns_,
             index_down_right += triangle_pixel_size_columns_)
        {
            int a = indices[i];
            int d = indices[index_down_right];
            int b = indices[index_right];
            int c = indices[index_down];

            if (isValidTriangle(a, c, b))
            {
                if (!isFilteredTriangle(a, c, b, mesh))
                {
                    addTriangle(a, c, b, idx++, mesh);
                }
            }

            if (isValidTriangle(b, c, d))
            {
                if (!isFilteredTriangle(b, c, d, mesh))
                {
                    addTriangle(b, c, d, idx++, mesh);
                }
            }
        }
    }
    mesh->triangles().resize(idx);
}

/////////////////////////////////////////////////////////////////////////////////////////////
template<class Filter>
void OrganizedFastMesher<Filter>::makeAdaptiveCutMesh(cv::Ptr<Mesh>          &mesh,
                                                      const cv::Size         &gridSize,
                                                      const std::vector<int> &indices)
{
    size_t width  = static_cast<size_t>(gridSize.width);
    size_t height = static_cast<size_t>(gridSize.height);

    size_t last_column = width - triangle_pixel_size_columns_;
    size_t last_row    = height - triangle_pixel_size_rows_;

    size_t i          = 0, index_down = 0, index_right = 0, index_down_right = 0, idx = 0;
    size_t y_big_incr = triangle_pixel_size_rows_ * width,
           x_big_incr = y_big_incr + triangle_pixel_size_columns_;

    // Reserve enough space
    mesh->triangles().resize(width * height * 2);

    // Go over the rows first
    for (size_t y = 0; y < last_row; y += triangle_pixel_size_rows_)
    {
        // Initialize a new row
        i                = y * width;
        index_right      = i + triangle_pixel_size_columns_;
        index_down       = i + y_big_incr;
        index_down_right = i + x_big_incr;

        // Go over the columns
        for (size_t x = 0; x < last_column; x += triangle_pixel_size_columns_,
             i += triangle_pixel_size_columns_,
             index_right += triangle_pixel_size_columns_,
             index_down += triangle_pixel_size_columns_,
             index_down_right += triangle_pixel_size_columns_)
        {
            int a = indices[i];
            int d = indices[index_down_right];
            int b = indices[index_right];
            int c = indices[index_down];

            const bool right_cut_upper = isValidTriangle(a, d, b);
            const bool right_cut_lower = isValidTriangle(a, c, d);
            const bool left_cut_upper  = isValidTriangle(a, c, b);
            const bool left_cut_lower  = isValidTriangle(b, c, d);

            if (right_cut_upper && right_cut_lower && left_cut_upper && left_cut_lower)
            {
                float dist_right_cut =
                    static_cast<float>(fabs(mesh->vertices()[size_t(c)][2] -
                                            mesh->vertices()[size_t(b)][2]));
                float dist_left_cut =
                    static_cast<float>(fabs(mesh->vertices()[size_t(a)][2] -
                                            mesh->vertices()[size_t(d)][2]));
                if (dist_right_cut >= dist_left_cut)
                {
                    if (!isFilteredTriangle(a, d, b, mesh))
                    {
                        addTriangle(a, d, b, idx++, mesh);
                    }
                    if (!isFilteredTriangle(a, c, d, mesh))
                    {
                        addTriangle(a, c, d, idx++, mesh);
                    }
                }
                else
                {
                    if (!isFilteredTriangle(a, c, b, mesh))
                    {
                        addTriangle(a, c, b, idx++, mesh);
                    }
                    if (!isFilteredTriangle(b, c, d, mesh))
                    {
                        addTriangle(b, c, d, idx++, mesh);
                    }
                }
            }
            else
            {
                if (right_cut_upper)
                {
                    if (!isFilteredTriangle(a, d, b, mesh))
                    {
                        addTriangle(a, d, b, idx++, mesh);
                    }
                }
                if (right_cut_lower)
                {
                    if (!isFilteredTriangle(a, c, d, mesh))
                    {
                        addTriangle(a, c, d, idx++, mesh);
                    }
                }
                if (left_cut_upper)
                {
                    if (!isFilteredTriangle(a, c, b, mesh))
                    {
                        addTriangle(a, c, b, idx++, mesh);
                    }
                }
                if (left_cut_lower)
                {
                    if (!isFilteredTriangle(b, c, d, mesh))
                    {
                        addTriangle(b, c, d, idx++, mesh);
                    }
                }
            }
        }
    }
    mesh->triangles().resize(idx);
}
