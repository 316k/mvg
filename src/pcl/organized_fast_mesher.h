/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Reimplementation of PCL's OrganizedFastMesher to support for custom filtering
 */

#ifndef SURFACE_ORGANIZED_FAST_MESH_H_
#define SURFACE_ORGANIZED_FAST_MESH_H_

#include <iostream>
#include <cassert>
#include <opencv2/core/core.hpp>
#include "../mesh.h"

/** \brief Simple triangulation/surface reconstruction for organized point
 * clouds. Neighboring points (pixels in image space) are connected to
 * construct a triangular (or quad) mesh.
 *
 * \note If you use this code in any academic work, please cite:
 *   D. Holz and S. Behnke.
 *   Fast Range Image Segmentation and Smoothing using Approximate Surface Reconstruction and Region
 *   Growing.
 *   In Proceedings of the 12th International Conference on Intelligent Autonomous Systems (IAS),
 *   Jeju Island, Korea, June 26-29 2012.
 *   <a href="http://purl.org/holz/papers/holz_2012_ias.pdf">
 *   http://purl.org/holz/papers/holz_2012_ias.pdf</a>
 *
 * \author Dirk Holz, Radu B. Rusu
 * \ingroup surface
 */
template <class Filter>
class OrganizedFastMesher
{
    public:
        enum TriangulationType
        {
            TRIANGLE_RIGHT_CUT,     // _always_ "cuts" a quad from top left to bottom right
            TRIANGLE_LEFT_CUT,      // _always_ "cuts" a quad from top right to bottom left
            TRIANGLE_ADAPTIVE_CUT  // "cuts" where possible and prefers larger differences in 'z'
                                   // direction
        };

        /** \brief Constructor. Triangulation type defaults to \a QUAD_MESH. */
        OrganizedFastMesher(const Filter &filter) :
            triangle_pixel_size_rows_(1),
            triangle_pixel_size_columns_(1),
            triangulation_type_(TRIANGLE_RIGHT_CUT),
            m_filter(filter)
        { }

        /** \brief Destructor. */
        virtual ~OrganizedFastMesher() { }

        /** \brief Set the edge length (in pixels) used for constructing the fixed mesh.
         * \param[in] triangle_size edge length in pixels
         * (Default: 1 = neighboring pixels are connected)
         */
        inline void
        setTrianglePixelSize(int triangle_size)
        {
            setTrianglePixelSizeRows(triangle_size);
            setTrianglePixelSizeColumns(triangle_size);
        }

        /** \brief Set the edge length (in pixels) used for iterating over rows when constructing
         * the fixed mesh.
         * \param[in] triangle_size edge length in pixels
         * (Default: 1 = neighboring pixels are connected)
         */
        inline void
        setTrianglePixelSizeRows(int triangle_size)
        {
            triangle_pixel_size_rows_ = std::max(1, (triangle_size - 1));
        }

        /** \brief Set the edge length (in pixels) used for iterating over columns when constructing
         * the fixed mesh.
         * \param[in] triangle_size edge length in pixels
         * (Default: 1 = neighboring pixels are connected)
         */
        inline void
        setTrianglePixelSizeColumns(int triangle_size)
        {
            triangle_pixel_size_columns_ = std::max(1, (triangle_size - 1));
        }

        /** \brief Set the triangulation type (see \a TriangulationType)
         * \param[in] type quad mesh, triangle mesh with fixed left, right cut,
         * or adaptive cut (splits a quad w.r.t. the depth (z) of the points)
         */
        inline void
        setTriangulationType(TriangulationType type)
        {
            triangulation_type_ = type;
        }

        /** \brief Perform the actual polygonal reconstruction.
         * !param[out] polygons the resultant polygons
         */
        void
        reconstruct(cv::Ptr<Mesh>          &mesh,
                    const cv::Size         &gridSize,
                    const std::vector<int> &indices);

    protected:
        /** \brief size of triangle edges (in pixels) for iterating over rows. */
        size_t triangle_pixel_size_rows_;

        /** \brief size of triangle edges (in pixels) for iterating over columns*/
        size_t triangle_pixel_size_columns_;

        /** \brief Type of meshing scheme (quads vs. triangles, left cut vs. right cut ... */
        TriangulationType triangulation_type_;

        Filter m_filter;

        /** \brief Add a new triangle to the current polygon mesh
         * \param[in] a index of the first vertex
         * \param[in] b index of the second vertex
         * \param[in] c index of the third vertex
         * \param[in] idx the index in the set of polygon vertices (assumes \a idx is valid in \a
         *            polygons)
         * !param[out] polygons the polygon mesh to be updated
         */
        inline void
        addTriangle(int            a,
                    int            b,
                    int            c,
                    size_t         idx,
                    cv::Ptr<Mesh> &mesh)
        {
            assert(idx < mesh->triangles().size());
            mesh->triangles()[idx][0] = static_cast<unsigned int>(a);
            mesh->triangles()[idx][1] = static_cast<unsigned int>(b);
            mesh->triangles()[idx][2] = static_cast<unsigned int>(c);
        }

        /** \brief Check if a triangle is valid.
         * \param[in] a index of the first vertex
         * \param[in] b index of the second vertex
         * \param[in] c index of the third vertex
         */
        inline bool
        isValidTriangle(const int &a,
                        const int &b,
                        const int &c)
        {
            return !(a < 0 || b < 0 || c < 0);
        }

        /** \brief Check if a triangle is filtered.
         * \param[in] a index of the first vertex
         * \param[in] b index of the second vertex
         * \param[in] c index of the third vertex
         */
        inline bool
        isFilteredTriangle(int                  a,
                           int                  b,
                           int                  c,
                           const cv::Ptr<Mesh> &mesh)
        {
            return m_filter.reject(mesh->vertices()[static_cast<size_t>(a)],
                                   mesh->vertices()[static_cast<size_t>(b)],
                                   mesh->vertices()[static_cast<size_t>(c)]);
        }

        /** \brief Create a right cut mesh.
         * !param[out] polygons the resultant mesh
         */
        void
        makeRightCutMesh(cv::Ptr<Mesh>          &mesh,
                         const cv::Size         &gridSize,
                         const std::vector<int> &indices);

        /** \brief Create a left cut mesh.
         * !param[out] polygons the resultant mesh
         */
        void
        makeLeftCutMesh(cv::Ptr<Mesh>          &mesh,
                        const cv::Size         &gridSize,
                        const std::vector<int> &indices);

        /** \brief Create an adaptive cut mesh.
         * !param[out] polygons the resultant mesh
         */
        void
        makeAdaptiveCutMesh(cv::Ptr<Mesh>          &mesh,
                            const cv::Size         &gridSize,
                            const std::vector<int> &indices);
};

#include "organized_fast_mesher.tpp"
#endif  // SURFACE_ORGANIZED_FAST_MESH_H_
