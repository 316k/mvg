/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Eigen/Core>
#include <algorithm>

template <typename T>
void skewMatrix(const T *const v,
                T             *M)
{
    // suppose that v is of dim 3 and M 3x3
    M[0 * 3 + 0] = T(0);  M[0 * 3 + 1] = -v[2]; M[0 * 3 + 2] = v[1];
    M[1 * 3 + 0] = v[2];  M[1 * 3 + 1] = T(0);  M[1 * 3 + 2] = -v[0];
    M[2 * 3 + 0] = -v[1]; M[2 * 3 + 1] = v[0];  M[2 * 3 + 2] = T(0);
}

template <typename T>
T fundamentalGeometricDistance(const T *const P,
                               const T *const X,
                               const T *const p,
                               const T *const q)
{
    T I[3 * 4];
    std::fill(I, I + 3 * 4, T(0));
    I[0 * 4 + 0] = I[1 * 4 + 1] = I[2 * 4 + 2] = T(1);

    T X4x1[4];
    std::copy(X, X + 3, X4x1);
    X4x1[3] = T(1);

    Eigen::Map< Eigen::Matrix<T, 3, 4, Eigen::RowMajor> > mI(const_cast<T *>(I));
    Eigen::Map< Eigen::Matrix<T, 3, 4, Eigen::RowMajor> > mP(const_cast<T *>(P));
    Eigen::Map< Eigen::Matrix<T, 4, 1> >                  mX(X4x1);

    T                                    Xc[3];
    Eigen::Map< Eigen::Matrix<T, 3, 1> > mXc(Xc);

    mXc = mI * mX;

    Xc[0] /= Xc[2];
    Xc[1] /= Xc[2];

    T d0 = (Xc[0] - p[0]) * (Xc[0] - p[0]) + (Xc[1] - p[1]) * (Xc[1] - p[1]);

    mXc    = mP * mX;
    Xc[0] /= Xc[2];
    Xc[1] /= Xc[2];

    T d1 = (Xc[0] - q[0]) * (Xc[0] - q[0]) + (Xc[1] - q[1]) * (Xc[1] - q[1]);

    return d0 + d1;
}

template <typename T>
T homographyGeometricDistance(const T *const H,
                              const T *const pe,
                              const T *const p,
                              const T *const q)
{
    T d0 = (pe[0] - p[0]) * (pe[0] - p[0]) + (pe[1] - p[1]) * (pe[1] - p[1]);

    T pec[3];
    std::copy(pe, pe + 2, pec);
    pec[2] = T(1);

    Eigen::Map< Eigen::Matrix<T, 3, 3, Eigen::RowMajor> > mH(const_cast<T *>(H));
    Eigen::Map< Eigen::Matrix<T, 3, 1> >                  mpe(pec);

    T                                    qc[3];
    Eigen::Map< Eigen::Matrix<T, 3, 1> > mqc(qc);

    mqc = mH * mpe;

    qc[0] /= qc[2];
    qc[1] /= qc[2];

    T d1 = (qc[0] - q[0]) * (qc[0] - q[0]) + (qc[1] - q[1]) * (qc[1] - q[1]);

    return d0 + d1;
}

template <typename T>
T resectionGeometricDistance(const T *const M,
                             const T *const P,
                             const T *const p)
{
    T P4x1[4];
    std::copy(P, P + 3, P4x1);
    P4x1[3] = T(1);

    Eigen::Map< Eigen::Matrix<T, 3, 4, Eigen::RowMajor> > mM(const_cast<T *>(M));
    Eigen::Map< Eigen::Matrix<T, 4, 1> >                  mP(P4x1);

    T                                    pc[3];
    Eigen::Map< Eigen::Matrix<T, 3, 1> > mpc(pc);

    mpc = mM * mP;

    pc[0] /= pc[2];
    pc[1] /= pc[2];

    T d = (pc[0] - p[0]) * (pc[0] - p[0]) + (pc[1] - p[1]) * (pc[1] - p[1]);

    return d;
}

template <typename T>
T sampsonDistanceToEpipolarLine(const T *const F,
                                const T *const p,
                                const T *const q)
{
    T a1, b1, c1, a2, b2, /*c2,*/ d, s;
    T norm, det;

    a1 = F[0] * p[0] + F[1] * p[1] + F[2];
    b1 = F[3] * p[0] + F[4] * p[1] + F[5];
    c1 = F[6] * p[0] + F[7] * p[1] + F[8];

    a2 = F[0] * q[0] + F[3] * q[1] + F[6];
    b2 = F[1] * q[0] + F[4] * q[1] + F[7];
    // c2 = F[2]*q[0] + F[5]*q[1] + F[8];

    d    = q[0] * a1 + q[1] * b1 + c1;
    norm = d * d;

    det = (a1 * a1 + b1 * b1 + a2 * a2 + b2 * b2);

    s = T(1) / det;

    return norm * s;
}
