/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits>
#include <ceres/ceres.h>

#include "lsvd.h"
#include "resect.h"
#include "mvg.h"

using namespace std;
using namespace cv;
using namespace ceres;

struct ResectionEstimator::Private
{
    Private();
    ~Private();
    Mat T1, T2; // normalization matrices
};

ResectionEstimator::Private::Private()
{
    T1.create(4, 4, CV_64F);
    T2.create(3, 3, CV_64F);
}

ResectionEstimator::Private::~Private()
{ }

/*
 * This file is largely inspired by OpenCV's version of algorithms for
 * fundamental estimation
 */

ResectionEstimator::ResectionEstimator(int modelPoints) :
    // size is flipped because it is width, height not rows, cols
    ModelEstimator(modelPoints, cv::Size(4, 3), 1)
{
    assert(modelPoints == 6);
    m_privImpl = new Private;
    setOptimize(false);
}

ResectionEstimator::~ResectionEstimator()
{
    delete m_privImpl;
}

void ResectionEstimator::initialize(const Mat &m1,
                                    const Mat &m2)
{
    m_privImpl->T1 = normalizationMatrix(m1);
    m_privImpl->T2 = normalizationMatrix(m2);
}

void ResectionEstimator::initialize(const Mat &m1,
                                    Size       size)
{
    m_privImpl->T1 = normalizationMatrix(m1);

    double scalex = 2.0 / size.width, scaley = 2.0 / size.height;
    (Mat_<double>(3, 3, m_privImpl->T2.ptr<double>()) <<
        scalex, 0, -1,
        0, scaley, -1,
        0, 0, 1);
}

/*virtual*/
int ResectionEstimator::runLSKernel(const Mat &m1,
                                    const Mat &m2,
                                    Mat       &model)
{
    const Point3d *p1 = m1.ptr<Point3d>();
    const Point2d *p2 = m2.ptr<Point2d>();
    assert((m1.cols == 1 || m1.rows == 1) && (m1.cols * m1.rows ==
                                              m2.rows * m2.cols));

    int count = m1.rows * m1.cols;
    Mat A(3 * count, 12, CV_64F);

    // don't need to compute centers and average distances, they are given in
    // the preconditionning parameters corresponding to the size of the image
    double sx0 = m_privImpl->T1.at<double>(0, 0), ox0 = m_privImpl->T1.at<double>(0, 3),
           sy0 = m_privImpl->T1.at<double>(1, 1), oy0 = m_privImpl->T1.at<double>(1, 3),
           sz0 = m_privImpl->T1.at<double>(2, 2), oz0 = m_privImpl->T1.at<double>(2, 3);
    double sx1 = m_privImpl->T2.at<double>(0, 0), ox1 = m_privImpl->T2.at<double>(0, 2),
           sy1 = m_privImpl->T2.at<double>(1, 1), oy1 = m_privImpl->T2.at<double>(1, 2);

    A = Scalar::all(0);
    // form a linear system Ax=0: for each selected pair of points m1 & m2, the
    // row of A(=a) represents the coefficients of equation: (m2, 1)'*F*(m1, 1)
    // = 0 to save computation time, we compute (At*A) instead of A and then
    // solve (At*A)x=0.
    for (int i = 0; i < count; i++)
    {
        double x0 = (p1[i].x * sx0) + ox0;
        double y0 = (p1[i].y * sy0) + oy0;
        double z0 = (p1[i].z * sz0) + oz0;

        double x1 = (p2[i].x * sx1) + ox1;
        double y1 = (p2[i].y * sy1) + oy1;

        Mat    Ai;
        double _r1[] = { 0, 0, 0, 0, -x0, -y0, -z0, -1, x0 * y1, y1 * y0, y1 * z0, y1 };
        Mat    r1(1, 12, CV_64F, _r1);
        Ai = A.row(i * 3);
        r1.copyTo(Ai);
        double _r2[] = { x0, y0, z0, 1, 0, 0, 0, 0, -x1 * x0, -x1 * y0, -x1 * z0, -x1 };
        Mat    r2(1, 12, CV_64F, _r2);
        Ai = A.row(i * 3 + 1);
        r2.copyTo(Ai);
        double _r3[] =
        { -x0 * y1, -y1 * y0, -y1 * z0, -y1, x1 * x0, x1 * y0, x1 * z0, x1, 0,
          0, 0, 0 };
        Mat r3(1, 12, CV_64F, _r3);
        Ai = A.row(i * 3 + 2);
        r3.copyTo(Ai);
    }

    LSVD lsvd(A, LSVD::MODIFY_A | LSVD::FULL_UV);

    double *_w = lsvd.w().ptr<double>();
    int     i;
    for (i = 0; i < 12; i++)
    {
        if (fabs(_w[i]) < numeric_limits<double>::epsilon())
        {
            break;
        }
    }
    if (i < 11) { return 0; }

    // take the last column of v as a solution of Af = 0
    Mat M = Mat(3, 4, CV_64F, lsvd.vt().ptr<double>(11));

    // apply the transformation that is inverse
    // to what we used to normalize the point coordinates
    model = m_privImpl->T2.inv() * M * m_privImpl->T1;

    return 1;
}

/*virtual*/
int ResectionEstimator::runKernel(const Mat &m1,
                                  const Mat &m2,
                                  Mat       &model)
{
    const Point3d *p1 = m1.ptr<Point3d>();
    const Point2d *p2 = m2.ptr<Point2d>();
    assert((m1.cols == 1 || m1.rows == 1) && (m1.cols * m1.rows ==
                                              m2.rows * m2.cols));

    int count = m1.rows * m1.cols;
    Mat A(11, 12, CV_64F);

    // don't need to compute centers and average distances, they are given in
    // the preconditionning parameters corresponding to the size of the image
    double sx0 = m_privImpl->T1.at<double>(0, 0), ox0 = m_privImpl->T1.at<double>(0, 3),
           sy0 = m_privImpl->T1.at<double>(1, 1), oy0 = m_privImpl->T1.at<double>(1, 3),
           sz0 = m_privImpl->T1.at<double>(2, 2), oz0 = m_privImpl->T1.at<double>(2, 3);
    double sx1 = m_privImpl->T2.at<double>(0, 0), ox1 = m_privImpl->T2.at<double>(0, 2),
           sy1 = m_privImpl->T2.at<double>(1, 1), oy1 = m_privImpl->T2.at<double>(1, 2);

    A = Scalar::all(0);
    // form a linear system Ax=0: for each selected pair of points m1 & m2, the
    // row of A(=a) represents the coefficients of equation: (m2, 1)'*F*(m1, 1)
    // = 0 to save computation time, we compute (At*A) instead of A and then
    // solve (At*A)x=0.
    for (int i = 0; i < count; i++)
    {
        double x0 = (p1[i].x * sx0) + ox0;
        double y0 = (p1[i].y * sy0) + oy0;
        double z0 = (p1[i].z * sz0) + oz0;

        double x1 = (p2[i].x * sx1) + ox1;
        double y1 = (p2[i].y * sy1) + oy1;

        Mat    Ai;
        double _r1[] = { 0, 0, 0, 0, -x0, -y0, -z0, -1, x0 * y1, y1 * y0, y1 * z0, y1 };
        Mat    r1(1, 12, CV_64F, _r1);
        Ai = A.row(i * 2);
        r1.copyTo(Ai);
        if (i == 5) { break; }

        double _r2[] = { x0, y0, z0, 1, 0, 0, 0, 0, -x1 * x0, -x1 * y0, -x1 * z0, -x1 };
        Mat    r2(1, 12, CV_64F, _r2);
        Ai = A.row(i * 2 + 1);
        r2.copyTo(Ai);
    }

    LSVD lsvd(A, LSVD::MODIFY_A | LSVD::FULL_UV);

    double *_w = lsvd.w().ptr<double>();
    int     i;
    for (i = 0; i < 12; i++)
    {
        if (fabs(_w[i]) < numeric_limits<double>::epsilon())
        {
            break;
        }
    }
    if (i < 11) { return 0; }

    // take the last column of v as a solution of Af = 0
    Mat M = Mat(3, 4, CV_64F, lsvd.vt().ptr<double>(11));

    // apply the transformation that is inverse
    // to what we used to normalize the point coordinates
    model = m_privImpl->T2.inv() * M * m_privImpl->T1;

    return 1;
}

/*virtual*/
void ResectionEstimator::computeReprojError(const Mat      &m1,
                                            const Mat      &m2,
                                            const Mat      &model,
                                            vector<double> &errors)
{
    size_t         count = static_cast<size_t>(m1.rows * m1.cols);
    const Point3d *p1    = m1.ptr<Point3d>();
    const Point2d *p2    = m2.ptr<Point2d>();
    const Mat     &M     = model;

    for (size_t i = 0; i < count; i++)
    {
        errors[i] = squaredDistanceToCameraProjection(M, p1[i], p2[i]);
    }
}

int ResectionEstimator::compressPoints(Mat                &m1,
                                       Mat                &m2,
                                       const vector<bool> &mask)
{
    assert((m1.cols == 1 || m1.rows == 1) &&
           (m1.cols * m1.rows == m2.rows * m2.cols));

    Point3d *m1ptr = m1.ptr<Point3d>();
    Point2d *m2ptr = m2.ptr<Point2d>();
    size_t   count = static_cast<size_t>(m1.cols * m1.rows);

    int j = 0;
    for (size_t i = 0; i < count; ++i)
    {
        if (mask[i])
        {
            m1ptr[j] = m1ptr[i];
            m2ptr[j] = m2ptr[i];
            ++j;
        }
    }

    if (m1.cols == 1)
    {
        m1.rows = m2.rows = j;
    }
    else
    {
        m1.cols = m2.cols = j;
    }

    return j;
}

bool ResectionEstimator::getSubset(const Mat &m1,
                                   const Mat &m2,
                                   Mat       &ms1,
                                   Mat       &ms2,
                                   int        maxAttempts)
{
    vector<int>    idx(static_cast<size_t>(modelPoints()));
    const Point3d *m1ptr  = m1.ptr<Point3d>();
    const Point2d *m2ptr  = m2.ptr<Point2d>();
    Point3d       *ms1ptr = ms1.ptr<Point3d>();
    Point2d       *ms2ptr = ms2.ptr<Point2d>();
    int            count  = m1.cols * m1.rows;

    size_t i;
    int    iters;
    // assert( CV_IS_MAT_CONT(m1->type & m2->type) && (elemSize % sizeof(int) == 0) );
    for (iters = 0, i = 0; i < static_cast<size_t>(modelPoints()) && iters < maxAttempts;)
    {
        idx[i] = static_cast<int>(rng()(static_cast<unsigned int>(count)));

        vector<int>::iterator idxE = idx.begin() + static_cast<int>(i);
        if (find(idx.begin(), idxE, idx[i]) != idxE) { continue; }

        ms1ptr[i] = m1ptr[idx[i]];
        ms2ptr[i] = m2ptr[idx[i]];

        if (!check3DSubset(ms1, static_cast<int>(i + 1)) ||
            !checkSubset(ms2, static_cast<int>(i + 1)))
        {
            iters++;
            continue;
        }

        i++;
    }

    return i == static_cast<size_t>(modelPoints()) && iters < maxAttempts;
}

bool ResectionEstimator::check3DSubset(const Mat &ms,
                                       int        count)
{
    if (count < 4) { return true; }

    Mat mst;
    if (ms.rows != 1) { mst = ms(Range(0, count), Range::all()); }
    else { mst = ms(Range::all(), Range(0, count)); }

    // check coplanarity of 3D points
    Scalar m = mean(mst);
    Mat    q = mst - m; q = q.reshape(1, count);
    Mat    t = q.t() * q;
    LSVD   lsvd(t, LSVD::MODIFY_A | LSVD::NO_UV);

    return (lsvd.w().at<double>(2, 0) / lsvd.w().at<double>(1, 0)) > 1E-3;
}

int findResectionMatrix(const Mat    &points1,
                        const Mat    &points2,
                        Mat          &mmatrix,
                        vector<bool> *mask,
                        int           method,
                        double        param1,
                        double        param2,
                        Size          size)
{
    int    result = 0;
    size_t count;

    double _M[3 * 4];
    Mat    M(3, 4, CV_64F, _M);

    Mat m1 = points1.clone(), m2 = points2.clone();
    assert(m1.rows * m1.cols == m2.rows * m2.cols);

    count = static_cast<size_t>(MAX(m1.cols, m1.rows));

    if (count < 6)
    {
        return 0;
    }

    vector<bool>       tempMask(count, true);
    ResectionEstimator estimator(6);

    if (size != Size()) { estimator.initialize(points1, size); }
    else { estimator.initialize(points1, points2); }

    if (count == 6)
    {
        result = estimator.runKernel(m1, m2, M);
    }
    else
    {
        if (param1 <= 0)
        {
            param1 = 3;
        }
        if ((param2 < DBL_EPSILON) || (param2 > 1 - DBL_EPSILON))
        {
            param2 = 0.99;
        }

        if (((method & ~3) == RM_LMEDS) && (count >= 2 * 6 + 1))
        {
            result = estimator.runLMeDS(m1, m2, M, tempMask, param2);
        }
        else
        {
            result = estimator.runRANSAC(m1, m2, M, tempMask, param1, param2);
        }

        if (result <= 0)
        {
            return 0;
        }
    }

    if (result) { M.copyTo(mmatrix); }

    if (mask)
    {
        mask->resize(count);
        copy(tempMask.begin(), tempMask.end(), mask->begin());
    }

    return result;
}

struct ResectionNonLinearEstimator
{
    ResectionNonLinearEstimator(double x_,
                                double y_) :
        x(x_),
        y(y_) { }

    template <typename T> bool operator()(const T *const M,
                                          const T *const P,
                                          T             *residuals) const
    {

        T p[2] = { T(x), T(y) };

        residuals[0] = ceres::sqrt(resectionGeometricDistance(M, P, p));

        return true;
    }

    double x, y;
};

/*virtual*/
bool ResectionEstimator::runOptimization(const Mat    &m1,
                                         const Mat    &m2,
                                         Mat          &model,
                                         vector<bool> &mask)
{
    size_t count = static_cast<size_t>(m1.rows * m1.cols);
    double sx0   = m_privImpl->T1.at<double>(0, 0), ox0 = m_privImpl->T1.at<double>(0, 3),
           sy0   = m_privImpl->T1.at<double>(1, 1), oy0 = m_privImpl->T1.at<double>(1, 3),
           sz0   = m_privImpl->T1.at<double>(2, 2), oz0 = m_privImpl->T1.at<double>(2, 3);
    double sx1   = m_privImpl->T2.at<double>(0, 0), ox1 = m_privImpl->T2.at<double>(0, 2),
           sy1   = m_privImpl->T2.at<double>(1, 1), oy1 = m_privImpl->T2.at<double>(1, 2);

    double paramsP[3 * 4];
    Mat    P(3, 4, CV_64F, paramsP);
    P = m_privImpl->T2 * model * m_privImpl->T1.inv();

    Mat mn1(static_cast<int>(count), 1, CV_64FC3),
    mn2(static_cast<int>(count), 1, CV_64FC2);
    const Point3d *p1      = m1.ptr<Point3d>();
    const Point2d *p2      = m2.ptr<Point2d>();
    double        *paramsX = mn1.ptr<double>();
    Point2d       *pn2     = mn2.ptr<Point2d>();

    Problem problem;
    for (size_t i = 0; i < count; ++i)
    {
        if (mask[i])
        {
            paramsX[i * 3]     = p1[i].x * sx0 + ox0;
            paramsX[i * 3 + 1] = p1[i].y * sy0 + oy0;
            paramsX[i * 3 + 2] = p1[i].z * sz0 + oz0;

            pn2[i].x = p2[i].x * sx1 + ox1;
            pn2[i].y = p2[i].y * sy1 + oy1;

            CostFunction *cost_function = new AutoDiffCostFunction<
                ResectionNonLinearEstimator, 1, 12, 3>(
                new ResectionNonLinearEstimator(pn2[i].x, pn2[i].y));

            /*
             *   Mat tX = (Mat_<double>(4,1) << paramsX[i*3+0],
             *        paramsX[i*3+1], paramsX[i*3+2], 1);
             *   Mat tp0 = M1*tX; tp0 /= tp0.at<double>(2,0);
             *   Mat tp1 = M2*tX; tp1 /= tp1.at<double>(2,0);
             *   Mat tp = (Mat_<double>(2,1) << pn1[i].x, pn1[i].y);
             *   Mat tq = (Mat_<double>(2,1) << pn2[i].x, pn2[i].y);
             *   cout << tX << tp0 << tp << tp1 << tq << endl;
             */

            problem.AddResidualBlock(cost_function, new HuberLoss(1),
                                     paramsP, paramsX + i * 3);
        }
    }

    Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_SCHUR;
    // options.ordering_type = ceres::SCHUR;
    // options.minimizer_progress_to_stdout = true;
    // options.num_linear_solver_threads = 2;
    // options.num_threads = 2;

    Solver::Summary summary;
    Solve(options, &problem, &summary);
    cout << summary.FullReport() << "\n";

    model = m_privImpl->T2.inv() * P * m_privImpl->T1;

    return true;
}

/* End of file. */

