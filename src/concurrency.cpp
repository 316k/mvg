/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * An implementation of OpenCV's parallel_for that only depends on OpenThread
 */

#include <vector>
#include <iostream>
#include <OpenThreads/Thread>
#include "concurrency.h"

using namespace std;
using namespace OpenThreads;

struct ParallelForThread : public Thread
{
    ParallelForThread() : m_id(gid++)
    {
        setProcessorAffinity(m_id);
    }
    virtual ~ParallelForThread();

    void setRange(size_t                 begin,
                  size_t                 end,
                  const ParallelForBody *body)
    {
        m_begin = begin;
        m_end   = end;
        m_body  = body;
    }

    void run()
    {
        if (m_body) { (*m_body)(m_begin, m_end); }
    }

    size_t                 m_begin, m_end;
    const ParallelForBody *m_body;
    unsigned int           m_id;
    static unsigned int    gid;
};

// forces out-of-line definition of virtual method
ParallelForThread::~ParallelForThread()
{ }

ParallelForBody::~ParallelForBody()
{ }

unsigned int ParallelForThread::gid = 0;

static vector<ParallelForThread *> getThreads()
{
    static vector<ParallelForThread *> threads;
    if (threads.size() == 0)
    {
        size_t numThreads = static_cast<size_t>(GetNumberOfProcessors());
        threads.resize(numThreads);
        for (size_t i = 0; i < numThreads; ++i)
        {
            threads[i] = new ParallelForThread;
            threads[i]->setSchedulePriority(OpenThreads::Thread::THREAD_PRIORITY_MAX);
        }
        Thread::SetConcurrency(static_cast<int>(numThreads));
        Thread::Init();
    }

    return threads;
}

void parallel_for(size_t                 begin,
                  size_t                 end,
                  const ParallelForBody &body)
{
    vector<ParallelForThread *> threads    = getThreads();
    size_t                      numThreads = threads.size();

    size_t count = (end - begin + numThreads - 1) / numThreads;
    for (size_t i = 0; i < numThreads; ++i)
    {
        threads[i]->setRange(begin + i * count,
                             min(begin + (i + 1) * count, end),
                             &body);
        threads[i]->start();
    }
    for (size_t i = 0; i < numThreads; ++i)
    {
        threads[i]->join();
    }
}
