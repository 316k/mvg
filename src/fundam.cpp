/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file is largely inspired by OpenCV's version of algorithms for
 * fundamental estimation
 */

#include <iostream>
#include <ceres/ceres.h>

#include "lsvd.h"
#include "fundam.h"
#include "mvg.h"

using namespace std;
using namespace cv;
using namespace ceres;

struct FundamentalEstimator::Private
{
    Private();
    ~Private();

    Mat T1, T2;
};

FundamentalEstimator::Private::Private()
{
    T1.create(3, 3, CV_64F);
    T2.create(3, 3, CV_64F);
}

FundamentalEstimator::Private::~Private()
{ }

FundamentalEstimator::FundamentalEstimator(int modelPoints) :
    ModelEstimator(modelPoints, cv::Size(3, 3), modelPoints == 7 ? 3 : 1)
{
    assert(modelPoints == 7 || modelPoints == 8);

    m_privImpl = new Private();
}

FundamentalEstimator::~FundamentalEstimator()
{
    delete m_privImpl;
}

/*virtual*/
void FundamentalEstimator::initialize(const Mat &m1,
                                      const Mat &m2,
                                      Size       size1,
                                      Size       size2)
{
    if (size1 != Size())
    {
        double scalex1 = 2.0 / size1.width, scaley1 = 2.0 / size1.height;
        (Mat_<double>(3, 3, m_privImpl->T1.ptr<double>()) <<
            scalex1, 0, -1,
            0, scaley1, -1,
            0, 0, 1);
    }
    else
    {
        m_privImpl->T1 = normalizationMatrix(m1);
    }

    if (size2 != Size())
    {
        double scalex2 = 2.0 / size2.width, scaley2 = 2.0 / size2.height;
        (Mat_<double>(3, 3, m_privImpl->T2.ptr<double>()) <<
            scalex2, 0, -1,
            0, scaley2, -1,
            0, 0, 1);
    }
    else
    {
        m_privImpl->T2 = normalizationMatrix(m2);
    }
}

/*virtual*/
int FundamentalEstimator::runLSKernel(const Mat &m1,
                                      const Mat &m2,
                                      Mat       &model)
{
    return run8Point(m1, m2, model);
}

/*virtual*/
int FundamentalEstimator::runKernel(const Mat &m1,
                                    const Mat &m2,
                                    Mat       &model)
{
    return modelPoints() == 7
           ? run7Point(m1, m2, model)
           : run8Point(m1, m2, model);
}

/*virtual*/
int FundamentalEstimator::run7Point(const Mat &m1,
                                    const Mat &m2,
                                    Mat       &model)
{
    double _A[7 * 9], _coeffs[4], _roots[3];
    Mat    A(7, 9, CV_64F, _A);
    Mat    coeffs(1, 4, CV_64F, _coeffs);
    Mat    roots(1, 3, CV_64F, _roots);

    const Point2d *p1 = m1.ptr<Point2d>(), *p2 = m2.ptr<Point2d>();
    model.create(9, 3, CV_64F);
    LSVD lsvd;

    // form a linear system: i-th row of A(=a) represents
    // the equation: (m2[i], 1)'*F*(m1[i], 1) = 0
    for (int i = 0; i < 7; i++)
    {
        double x0 = p1[i].x, y0 = p1[i].y;
        double x1 = p2[i].x, y1 = p2[i].y;

        _A[i * 9 + 0] = x1 * x0;
        _A[i * 9 + 1] = x1 * y0;
        _A[i * 9 + 2] = x1;
        _A[i * 9 + 3] = y1 * x0;
        _A[i * 9 + 4] = y1 * y0;
        _A[i * 9 + 5] = y1;
        _A[i * 9 + 6] = x0;
        _A[i * 9 + 7] = y0;
        _A[i * 9 + 8] = 1;
    }

    // A*(f11 f12 ... f33)' = 0 is singular (7 equations for 9 variables), so
    // the solution is linear subspace of dimensionality 2.
    // => use the last two singular vectors as a basis of the space
    // (according to SVD properties)
    /*
     * svd(A, SVD::MODIFY_A | SVD::FULL_UV);
     *   double *f1 = svd.vt.ptr<double>(7), *f2 = svd.vt.ptr<double>(8);
     */
    lsvd(A, SVD::MODIFY_A | SVD::FULL_UV);
    double *f1 = lsvd.vt().ptr<double>(7), *f2 = lsvd.vt().ptr<double>(8);

    // f1, f2 is a basis => lambda*f1 + mu*f2 is an arbitrary f. matrix.
    // as it is determined up to a scale, normalize lambda & mu (lambda + mu =
    // 1), so f ~ lambda*f1 + (1 - lambda)*f2.
    // use the additional constraint det(f) = det(lambda*f1 + (1-lambda)*f2) to
    // find lambda.
    // it will be a cubic equation.
    // find c - polynomial coefficients.
    for (int i = 0; i < 9; i++)
    {
        f1[i] -= f2[i];
    }

    double t0, t1, t2;
    t0 = f2[4] * f2[8] - f2[5] * f2[7];
    t1 = f2[3] * f2[8] - f2[5] * f2[6];
    t2 = f2[3] * f2[7] - f2[4] * f2[6];

    _coeffs[3] = f2[0] * t0 - f2[1] * t1 + f2[2] * t2;

    _coeffs[2] = f1[0] * t0 - f1[1] * t1 + f1[2] * t2 -
                 f1[3] * (f2[1] * f2[8] - f2[2] * f2[7]) +
                 f1[4] * (f2[0] * f2[8] - f2[2] * f2[6]) -
                 f1[5] * (f2[0] * f2[7] - f2[1] * f2[6]) +
                 f1[6] * (f2[1] * f2[5] - f2[2] * f2[4]) -
                 f1[7] * (f2[0] * f2[5] - f2[2] * f2[3]) +
                 f1[8] * (f2[0] * f2[4] - f2[1] * f2[3]);

    t0 = f1[4] * f1[8] - f1[5] * f1[7];
    t1 = f1[3] * f1[8] - f1[5] * f1[6];
    t2 = f1[3] * f1[7] - f1[4] * f1[6];

    _coeffs[1] = f2[0] * t0 - f2[1] * t1 + f2[2] * t2 -
                 f2[3] * (f1[1] * f1[8] - f1[2] * f1[7]) +
                 f2[4] * (f1[0] * f1[8] - f1[2] * f1[6]) -
                 f2[5] * (f1[0] * f1[7] - f1[1] * f1[6]) +
                 f2[6] * (f1[1] * f1[5] - f1[2] * f1[4]) -
                 f2[7] * (f1[0] * f1[5] - f1[2] * f1[3]) +
                 f2[8] * (f1[0] * f1[4] - f1[1] * f1[3]);

    _coeffs[0] = f1[0] * t0 - f1[1] * t1 + f1[2] * t2;

    // solve the cubic equation; there can be 1 to 3 roots ...
    int n = solveCubic(coeffs, roots);

    if ((n < 1) || (n > 3))
    {
        return n;
    }

    double *fmatrix = model.ptr<double>();
    for (int k = 0; k < n; k++, fmatrix += 9)
    {
        // for each root form the fundamental matrix
        double lambda = _roots[k], mu = 1.;
        double s      = f1[8] * _roots[k] + f2[8];

        // normalize each matrix, so that F(3,3) (~fmatrix[8]) == 1
        if (fabs(s) > numeric_limits<double>::epsilon())
        {
            mu         = 1. / s;
            lambda    *= mu;
            fmatrix[8] = 1.;
        }
        else
        {
            fmatrix[8] = 0.;
        }

        for (int i = 0; i < 8; i++)
        {
            fmatrix[i] = f1[i] * lambda + f2[i] * mu;
        }
    }

    return n;
}

/*virtual*/
int FundamentalEstimator::run8Point(const Mat &m1,
                                    const Mat &m2,
                                    Mat       &model)
{
    const Point2d *p1 = m1.ptr<Point2d>(), *p2 = m2.ptr<Point2d>();
    assert((m1.cols == 1 || m1.rows == 1) &&
           (m1.cols * m1.rows == m2.rows * m2.cols));

    int count = m1.rows * m1.cols;
    Mat A(count, 9, CV_64F), Ai;

    // don't need to compute centers and average distances, they are given in
    // the preconditionning parameters corresponding to the size of the image
    double sx0 = m_privImpl->T1.at<double>(0, 0), ox0 = m_privImpl->T1.at<double>(0, 2),
           sy0 = m_privImpl->T1.at<double>(1, 1), oy0 = m_privImpl->T1.at<double>(1, 2);
    double sx1 = m_privImpl->T2.at<double>(0, 0), ox1 = m_privImpl->T2.at<double>(0, 2),
           sy1 = m_privImpl->T2.at<double>(1, 1), oy1 = m_privImpl->T2.at<double>(1, 2);

    A = Scalar::all(0);

    // form a linear system Ax=0: for each selected pair of points m1 & m2, the
    // row of A(=a) represents the coefficients of equation: (m2, 1)'*F*(m1, 1)
    // = 0 to save computation time, we compute (At*A) instead of A and then
    // solve (At*A)x=0.
    for (int i = 0; i < count; i++)
    {

        double x0 = (p1[i].x * sx0) + ox0;
        double y0 = (p1[i].y * sy0) + oy0;

        double x1 = (p2[i].x * sx1) + ox1;
        double y1 = (p2[i].y * sy1) + oy1;

        double _r[9] = { x1 *x0, x1 * y0, x1, y1 * x0, y1 * y0, y1, x0, y0, 1 };
        Mat    r(1, 9, CV_64F, _r);
        Ai = A.row(i);
        r.copyTo(Ai);
    }

    // SVD svd(A, SVD::MODIFY_A | SVD::FULL_UV);
    LSVD lsvd(A, SVD::MODIFY_A | SVD::FULL_UV);

    // double *_w = svd.w.ptr<double>();
    double *_w = lsvd.w().ptr<double>();
    int     i;
    for (i = 0; i < 9; i++)
    {
        if (fabs(_w[i]) < numeric_limits<double>::epsilon())
        {
            break;
        }
    }

    if (i < 8) { return 0; }

    Mat F = Mat(3, 3, CV_64F, lsvd.vt().ptr<double>(8)); // take the last column of v as a
    // solution of Af = 0
    // make F singular (of rank 2) by decomposing it with SVD,
    // zeroing the last diagonal element of W and then composing the matrices
    // back.
    rank2MatrixSVDCorrection(F, F);

    // apply the transformation that is inverse
    // to what we used to normalize the point coordinates
    model = m_privImpl->T2.t() * F * m_privImpl->T1;
    // make F(3,3) = 1
    if (fabs(model.at<double>(2, 2)) > numeric_limits<double>::epsilon())
    {
        model /= model.at<double>(2, 2);
    }

    return 1;
}

/*virtual*/
void FundamentalEstimator::computeReprojError(const Mat      &m1,
                                              const Mat      &m2,
                                              const Mat      &model,
                                              vector<double> &errors)
{
    size_t         count = static_cast<size_t>(m1.rows * m1.cols);
    const Point2d *p1    = m1.ptr<Point2d>(), *p2 = m2.ptr<Point2d>();

    const Mat &F  = model;
    Mat        tF = F.t();

    for (size_t i = 0; i < count; i++)
    {
        errors[i] = max(squaredDistanceToEpipolarLine(tF, p2[i], p1[i]),
                        squaredDistanceToEpipolarLine(F, p1[i], p2[i]));
    }
}

int findFundamentalMatrix(const Mat    &points1,
                          const Mat    &points2,
                          Mat          &fmatrix,
                          vector<bool> *mask,
                          int           method,
                          double        param1,
                          double        param2,
                          Size          size1,
                          Size          size2)
{
    int    result = 0;
    size_t count;

    double _F[9 * 3];
    Mat    F3x3(3, 3, CV_64FC1, _F),
    F9x3(9, 3, CV_64FC1, _F);

    Mat m1 = points1.clone(), m2 = points2.clone();
    assert(m1.rows * m1.cols == m2.rows * m2.cols);

    count = static_cast<size_t>(MAX(m1.cols, m1.rows));
    if (count < 7)
    {
        return 0;
    }

    vector<bool>         tempMask(count, true);
    FundamentalEstimator estimator(7);

    estimator.initialize(m1, m2, size1, size2);

    if (count == 7)
    {
        result = estimator.run7Point(m1, m2, F9x3);
    }
    else if (method == FM_8POINT)
    {
        result = estimator.run8Point(m1, m2, F3x3);
    }
    else
    {
        if (param1 <= 0)
        {
            param1 = 3;
        }
        if ((param2 < DBL_EPSILON) || (param2 > 1 - DBL_EPSILON))
        {
            param2 = 0.99;
        }

        if (((method & ~3) == FM_LMEDS) && (count >= 2 * 7 + 1))
        {
            result = estimator.runLMeDS(m1, m2, F3x3, tempMask, param2);
        }
        else
        {
            result = estimator.runRANSAC(m1, m2, F3x3, tempMask, param1, param2);
        }

        if (result <= 0)
        {
            return 0;
        }
    }

    if (result)
    {
        if (count == 7) { F9x3.copyTo(fmatrix); }
        else { F3x3.copyTo(fmatrix); }
    }

    if (mask)
    {
        mask->resize(count);
        copy(tempMask.begin(), tempMask.end(), mask->begin());
    }

    return result;
}

struct FundamentalNonLinearEstimator
{
    FundamentalNonLinearEstimator(double x0_,
                                  double y0_,
                                  double x1_,
                                  double y1_) :
        x0(x0_),
        y0(y0_),
        x1(x1_),
        y1(y1_) { }

    template <typename T> bool operator()(const T *const P,
                                          const T *const X,
                                          T             *residuals) const
    {

        T p[2] = { T(x0), T(y0) };
        T q[2] = { T(x1), T(y1) };

        residuals[0] = ceres::sqrt(fundamentalGeometricDistance(P, X, p, q));

        return true;
    }

    double x0, y0;
    double x1, y1;
};

/*virtual*/
bool FundamentalEstimator::runOptimization(const Mat    &m1,
                                           const Mat    &m2,
                                           Mat          &model,
                                           vector<bool> &mask)
{
    size_t count = static_cast<size_t>(m1.rows * m1.cols);
    double sx0   = m_privImpl->T1.at<double>(0, 0), ox0 = m_privImpl->T1.at<double>(0, 2),
           sy0   = m_privImpl->T1.at<double>(1, 1), oy0 = m_privImpl->T1.at<double>(1, 2);
    double sx1   = m_privImpl->T2.at<double>(0, 0), ox1 = m_privImpl->T2.at<double>(0, 2),
           sy1   = m_privImpl->T2.at<double>(1, 1), oy1 = m_privImpl->T2.at<double>(1, 2);

    double paramsP[3 * 4];
    Mat    M1, M2(3, 4, CV_64F, paramsP);
    projectiveCamerasFromFundamentalMatrix(
        m_privImpl->T2.t().inv() * model * m_privImpl->T1.inv(),
        M1, M2);

    Mat            mn1(int(count), 1, CV_64FC2), mn2(int(count), 1, CV_64FC2);
    const Point2d *p1  = m1.ptr<Point2d>(), *p2 = m2.ptr<Point2d>();
    Point2d       *pn1 = mn1.ptr<Point2d>(), *pn2 = mn2.ptr<Point2d>();
    for (size_t i = 0; i < count; ++i)
    {
        pn1[i].x = (p1[i].x * sx0) + ox0;
        pn1[i].y = (p1[i].y * sy0) + oy0;

        pn2[i].x = (p2[i].x * sx1) + ox1;
        pn2[i].y = (p2[i].y * sy1) + oy1;
    }

    vector<Mat> cams(2);
    cams[0] = M1; cams[1] = M2;
    vector<Mat> points(2);
    points[0] = mn1; points[1] = mn2;
    Mat vmask(2, int(count), CV_8U);
    vmask = Scalar::all(1);
    Mat Xs;
    triangulatePoints(points, cams, vmask,
                      Size(static_cast<int>(1 / m_privImpl->T1.at<double>(0, 0)),
                           static_cast<int>(1 / m_privImpl->T1.at<double>(1, 1))),
                      Xs);
    double *paramsX = Xs.ptr<double>();

    Problem problem;
    for (size_t i = 0; i < count; ++i)
    {
        if (mask[i])
        {
            CostFunction *cost_function = new AutoDiffCostFunction<
                FundamentalNonLinearEstimator, 1, 12, 3>(
                new FundamentalNonLinearEstimator(pn1[i].x, pn1[i].y,
                                                  pn2[i].x, pn2[i].y));

            /*
             *   Mat tX = (Mat_<double>(4,1) << paramsX[i*3+0],
             *        paramsX[i*3+1], paramsX[i*3+2], 1);
             *   Mat tp0 = M1*tX; tp0 /= tp0.at<double>(2,0);
             *   Mat tp1 = M2*tX; tp1 /= tp1.at<double>(2,0);
             *   Mat tp = (Mat_<double>(2,1) << pn1[i].x, pn1[i].y);
             *   Mat tq = (Mat_<double>(2,1) << pn2[i].x, pn2[i].y);
             *   cout << tX << tp0 << tp << tp1 << tq << endl;
             */

            problem.AddResidualBlock(cost_function, new HuberLoss(1),
                                     paramsP, paramsX + i * 3);
        }
    }

    Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_SCHUR;
    // options.ordering_type = ceres::SCHUR;
    // options.minimizer_progress_to_stdout = true;
    // options.num_linear_solver_threads = 2;
    // options.num_threads = 2;

    Solver::Summary summary;
    Solve(options, &problem, &summary);
    cout << summary.FullReport() << "\n";

    Mat M2s3x3 = M2(Range::all(), Range(0, 3)),
        M2s3x1 = M2(Range::all(), Range(3, 4));

    Mat vm = M2s3x1.clone(), mX;
    skewMatrix(vm, mX);

    model = m_privImpl->T2.t() * mX * M2s3x3 * m_privImpl->T1;

    return true;
}

/* End of file. */

