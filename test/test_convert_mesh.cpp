/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <xtclap/CmdLine.h>

#include "../src/mesh.h"
#include "../src/logger.h"

using namespace cv;
using namespace std;
using namespace TCLAP;

int main(int   argc,
         char *argv[])
{

    CmdLine cmd("Test mesh conversion");

    SwitchArg asciiArg("a", "ascii", "Export mesh with ascii encoding",
                       cmd);
    UnlabeledMultiArg<string> fileNameArgs("filenames", "Input and output file name",
                                           true, "string", cmd);

    cmd.parse(argc, argv);

    bool           ascii     = asciiArg.getValue();
    vector<string> fileNames = fileNameArgs.getValue();

    if (fileNames.size() != 2)
    {
        logError() << "USAGE:\n\ttest_convert_mesh in.ply out.ply";
    }

    Mesh in, out;
    if (!in.read(fileNames[0]))
    {
        logError() << "Could not read" << fileNames[0];
    }
    out = in;
    if (!out.write(fileNames[1], ascii))
    {
        logError() << "Could not write" << fileNames[1];
    }
    if (!out.read(fileNames[1]))
    {
        logError() << "Could not read" << fileNames[1];
    }

    size_t nvertsI = in.vertices().size();
    size_t nvertsO = out.vertices().size();
    if (nvertsI != nvertsO)
    {
        logError() << "#vertices mismatch" << nvertsO << "vs" << nvertsI;
    }
    for (size_t i = 0; i < nvertsI; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            if (std::abs(in.vertices()[i][j] - out.vertices()[i][j]) > 0.001)
            {
                logError() << "#vertices mismatch at position" << i << j;
            }
        }
    }

    size_t ntrianglesI = in.triangles().size();
    size_t ntrianglesO = out.triangles().size();
    if (ntrianglesI != ntrianglesO)
    {
        logError() << "#triangles mismatch" << ntrianglesO << "vs" << ntrianglesI;
    }
    for (size_t i = 0; i < ntrianglesI; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            if (in.triangles()[i][j] != out.triangles()[i][j])
            {
                logError() << "#triangles mismatch at position" << i << j;
            }
        }
    }

    size_t ntexCoordsI = in.texCoords().size();
    size_t ntexCoordsO = out.texCoords().size();
    if (ntexCoordsI != ntexCoordsO)
    {
        logError() << "#texCoords mismatch" << ntexCoordsO << "vs" << ntexCoordsI;
    }
    for (size_t i = 0; i < ntexCoordsI; ++i)
    {
        for (int j = 0; j < 2; ++j)
        {
            if (std::abs(in.texCoords()[i][j] - out.texCoords()[i][j]) > 0.00001)
            {
                logError() << "#texCoords mismatch at position" << i << j;
            }
        }
    }

    size_t nnormalsI = in.normals().size();
    size_t nnormalsO = out.normals().size();
    if (nnormalsI != nnormalsO)
    {
        logError() << "#normals mismatch" << nnormalsO << "vs" << nnormalsI;
    }
    for (size_t i = 0; i < nnormalsI; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            if (std::abs(in.normals()[i][j] - out.normals()[i][j]) > 0.00001)
            {
                logError() << "#normals mismatch at position" << i << j;
            }
        }
    }

    size_t ncolorsI = in.colors().size();
    size_t ncolorsO = out.colors().size();
    if (ncolorsI != ncolorsO)
    {
        logError() << "#colors mismatch" << ncolorsO << "vs" << ncolorsI;
    }
    for (size_t i = 0; i < ncolorsI; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            if (in.colors()[i][j] != out.colors()[i][j])
            {
                logError() << "#colors mismatch at position" << i << j;
            }
        }
    }

    return 0;
}
