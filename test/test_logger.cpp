/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include "../src/logger.h"

using namespace std;

bool infiniteLoop()
{
    for (;;)
    { }

    return true;
}

int main(int   argc,
         char *argv[])
{
    (void)argc;
    (void)argv;

    Logger::verbosityThreshold = Logger::L_ERROR;
    cout << "Avec espaces et headers level error" << endl;
    logInfo() << "salut" << "!" << 3 << 18.5;
    logWarning() << "salut" << "!" << 3 << 18.5;
    logDebug() << "salut" << "!" << 3 << 18.5;

    cout << "Avec espaces et headers level info" << endl;
    Logger::verbosityThreshold = Logger::L_INFO;
    logInfo() << "salut" << "!" << 3 << 18.5;
    logWarning() << "salut" << "!" << 3 << 18.5;
    logDebug() << "salut" << "!" << 3 << 18.5;

    cout << "Sans espaces et headers level info" << endl;
    logInfo().noSpace() << "salut" << "!" << 3 << 18.5;
    logWarning().noSpace() << "salut" << "!" << 3 << 18.5;
    logDebug().noSpace() << "salut" << "!" << 3 << 18.5;

    cout << "Avec espaces et sans headers level debug" << endl;
    Logger::verbosityThreshold = Logger::L_DEBUG;
    logInfo(false) << "salut" << "!" << 3 << 18.5;
    logWarning(false) << "salut" << "!" << 3 << 18.5;
    logDebug(false) << "salut" << "!" << 3 << 18.5;

    cout << "Sans espaces et sans headers level debug" << endl;
    logInfo(false) << nospace << "salut" << "!" << 3 << 18.5;
    logWarning(false) << nospace << "salut" << "!" << 3 << 18.5;
    logDebug(false) << nospace << "salut" << "!" << 3 << 18.5;

    cout << "Test de manipulateurs" << endl;
    logDebug(false) << 10 << hex << 10 << endl << 10;
    logDebug() << "yo" << 3.14 << 10 << nospace << dec << "yo" << 3.14 << 10;

    if (1 > 2)
    {
        logInfo() << "1 > 2";
    }
    else
    {
        logInfo() << "OUFF..";
    }

    ProgressLogger logProg = logProgress();
    for (int i = 0; i < 100; ++i)
    {
        logProg++;
    }

    logInfo() << "Done!";

    // infiniteLoop n'est jamais evalue car le niveau de verbosite est plus grand que info
    Logger::verbosityThreshold = Logger::L_WARNING;
    logInfo() << infiniteLoop();

    Logger::verbosityThreshold = Logger::L_DEBUG;
    // en plus, en mode release, logDebug est un noop et donc ne coute rien a appeler (i.e meme pas
    // un test pour verifier si le niveau de verbosite est adequat!)
    logInfo() << "info est evalue selon le niveau de verbosite";
    logDebug()
        << "debug est evalue seulement si en mode debug et le niveau de verbosite est OK";

    logError(true, false, std::cerr) << "toto" << "salut";

    logInfo() << "JAMAIS AFFICHE!!";
}
