/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "../src/mvg.h"
#include "../src/logger.h"

using namespace cv;
using namespace std;

int main(int   argc,
         char *argv[])
{
    (void)argc;
    (void)argv;
    CameraGeometricParameters cam1;
    cam1.setImageSize(Size(1292, 964));
    cam1.setDistorsionCoefficients((Mat_<double>(4, 1) << 0.1, -0.087, 0.01, -0.01));
    cam1.setIntrinsicMatrix((Mat_<double>(3, 3) <<
                             2460.6113533003922, 1.2789747327112961e-14,
                             674.7354571009251,
                             0., 2454.58812587038, 461.33088715669,
                             0., 0., 1.));
    cam1.setRotationMatrix((Mat_<double>(3, 3) <<
                            -0.9230856066523585, -0.00804039233270679,
                            -0.38451048735032367,
                            0.006115379049693896, -0.9999619009041676,
                            0.006228874633660744,
                            -0.3845459204442635, 0.0033983571412444557,
                            0.9230996079721921));
    cam1.setTranslationVector((Mat_<double>(3, 1) <<
                               10.977004783757778, 8.450781543960538,
                               60.033410106373026));
    Mat M1 = (Mat_<double>(3, 4) <<
              -2530.821691204229, -17.49128859975799, -323.2828347024715,
              67516.813005598, -162.3921738304629, -2452.926841167298,
              441.143682793123, 48438.45437554455, -0.3845459204442634,
              0.003398357141244455, 0.9230996079721918, 60.03341010637301);
    (void)M1;
    assert(norm(cam1.M(), M1) < 1E-4);

    CameraGeometricParameters cam2;
    cam2.setImageSize(Size(1280, 720));
    cam2.setDistorsionCoefficients((Mat_<double>(4, 1) << -0.09, 0.08, -0.01, 0.01));
    cam2.setIntrinsicMatrix((Mat_<double>(3, 3) <<
                             1941.8719931939575, 0., 653.564094012225,
                             0., 1743.6477031700124, 703.1872786762812,
                             0., 0., 1.));
    cam2.setRotationMatrix((Mat_<double>(3, 3) <<
                            -0.9959736854996757, -0.011526430713194816,
                            -0.08890196391085754,
                            0.013664670145213446, -0.9996308998996967,
                            -0.02348064640390068,
                            -0.08859850214318529, -0.024600921948911204,
                            0.9957635763861056));
    cam2.setTranslationVector((Mat_<double>(3, 1) <<
                               6.218982652365888, -9.52445224710112, 71.67376331619843));
    Mat M2 = (Mat_<double>(3, 4) <<
              -1991.958205614037, -38.46113224884927, 478.1590857927239,
              58919.86642498626, -38.47496890358386, -1760.303177986061,
              659.2663043127981, 33792.78929419511, -0.08859850214318528,
              -0.0246009219489112, 0.9957635763861055, 71.67376331619842);
    (void)M2;
    assert(norm(cam2.M(), M2) < 1E-4);

    Mat points1 = (Mat_<double>(10, 2) <<
                   955.8596227826599, 610.8610559621613, 1010.2023387879414,
                   413.66721491665646, 927.6205012780164, 477.1821569257453,
                   1163.4337013316176, 617.0433730907148, 779.0658980040805,
                   786.0930901247225, 770.6005370561478, 938.1460306661679,
                   1160.5701204339239, 899.8772860226236, 874.3615428006487,
                   892.9035282392924, 1232.2599466599404, 525.5332199066678,
                   718.9094278569975, 634.458264960502);
    points1 = points1.reshape(2);

    Mat points2 = (Mat_<double>(10, 2) <<
                   797.9495985581909, 399.595502175239, 831.6234219297247,
                   275.43832210187503, 736.8326842754833, 299.12049352042624,
                   953.1551132364724, 403.7403406385873, 643.8564459008085,
                   486.4311305888874, 639.364650557489, 574.4236092703435,
                   947.149487913633, 577.4870575881384, 729.5328726065237,
                   561.9308273895488, 1007.6703137713102, 346.3535564521478,
                   621.2385964311698, 407.6135726177334);
    points2 = points2.reshape(2);

    Mat pts3d = (Mat_<double>(10, 3) <<
                 -0.6703266628091313, 4.311106762869315, 9.519232184182197,
                 -1.9554534839477142, 9.842572277122429, 8.594557717982477,
                 2.4118396705603473, 8.091885208528318, 5.414591289071735,
                 -7.055832236475219, 4.004563262980561, 8.77535489156154,
                 6.021503105330666, 0.08215277467332527, 7.00678390730031,
                 6.229572123153659, -3.833082802603446, 7.145775334004934,
                 -6.721575245756171, -4.0182431150930675, 8.626736126036569,
                 2.354378161900044, -3.2121803813556653, 8.790299637502624,
                 -9.322178365181706, 6.614961930737103, 8.569533362558726,
                 6.739450602100831, 3.9085924902457165, 9.203635159320328);
    pts3d = pts3d.reshape(3);

    /*
     *   Mat p1t, p2t;
     *   projectPoints(pts3d, cam1.R, cam1.t, cam1.K, cam1.coeffs, p1t);
     *   logInfo() << p1t; //points avec distorsion calculés par OpenCV
     *   logInfo() << points1; //points avec disto calculés par Mathematica
     *   undistortPoints(p1t, p2t, cam1.K, cam1.coeffs, Mat(), cam1.K);
     *   projectPoints(pts3d, cam1.R, cam1.t, cam1.K, Mat(), p1t);
     *   logInfo() << p1t; //points dé-distortionnés
     *   logInfo() << p2t; //points projettés sans disto
     *   exit(0);
     */

    Mat pts3d_;
    triangulatePoints(points1, points2, cam1, cam2, pts3d_, true);

    Point3d *p1 = pts3d.ptr<Point3d>(),
            *p2 = pts3d_.ptr<Point3d>();
    for (size_t i = 0; i < 10; ++i)
    {
        cout << p1[i] << p2[i] << endl;
    }

    return 0;
}
