#!/bin/bash
# ---------------------------------------------------------------------------
# build.sh - Build and install MVG from source

# Copyright 2015,  Nicolas Martin (nicolas.martin.3d@gmail.com)
# All rights reserved.

# Usage: build.sh [-h|--help] [--install-path install_path] [--source-path
# source_path] -l|--library-type type

# Revision history:
# 2015-06-09 Created by new_script ver. 3.3
# ---------------------------------------------------------------------------

# Parse command-line
while [[ -n $1 ]]; do
  case $1 in
    -h | --help)
      help_message; graceful_exit ;;
    --install-path)
      echo "Install path"; shift; install-path="$1" ;;
    --build-path)
      echo "Build path"; shift; build-path="$1" ;;
    -l | --library-type)
      echo "Type of library to build and install"; shift; type="$1" ;;
    -* | --*)
      usage
      error_exit "Unknown option $1" ;;
    *)
      echo "Argument $1 to process..." ;;
  esac
  shift
done

# Main logic
if [[ -n "$install_path" ]]; then
    if [[ ! -d "$install_path" ]]; then
        error_exit "$install_path does not exist or is not a folder";
    fi
else
    install_path="/usr/local/lib"
fi

if [[ -n "$build_path" ]]; then
    if [[ ! -d "$build_path" ]]; then
        error_exit "$build_path does not exist or is not a folder";
    fi
else
    build_path="$(pwd)/../build"
fi

if [[ "$type" == "static" ]]; then
    build_shared="OFF"
elif [[ "$type" == "shared" ]]; then
    build_shared="ON"
else
    error_exit "Library type should either be 'static' or 'shared'";
fi

echo -e "Building MVG from source using :"
echo -e "--build-path=$build_path"
echo -e "--install-path=$install_path"
echo -e "--library-type=$type"

graceful_exit

source_path="$(pwd)/../"

mkdir -p $build_path
cd $build_path

c=linux_${type}_x64
rm -rf $c
mkdir -p $c
cd $c

p=${install_path}
cmake -DCMAKE_INSTALL_PREFIX=$p \
 -DOpenCV_DIR=$p/share \
 -DCeres_DIR=$p/share \
 -DXTCLAP_INCLUDE_DIR=$p/include \
 -DBUILD_TEST=ON \
 -DBUILD_APPLICATIONS=ON \
 -DBUILD_SHARED_LIBS=${build_shared} \
 ${source_path}

make -j4
if [[ -w $install_path ]]; then
    make install
else
    sudo make install
fi

cd ..

graceful_exit

clean_up() { # Perform pre-exit housekeeping
  return
}

error_exit() {
  echo -e "${1:-"Unknown Error"}" >&2
  clean_up
  exit 1
}

graceful_exit() {
  clean_up
  exit
}

usage() {
  echo -e "Usage: ./build.sh [-h|--help] [--install-path path] [--build-path path] -l|--library-type type"
  echo -e "Example usage : ./build.sh --install-path /home/CodedLight_v.2.1/Install/linux/shared/Release/x64 -l static"
}

help_message() {
  cat <<- _EOF_
  Build and install MVG from source

  $(usage)

  Options:
  -h, --help  Display this help message and exit.
  --install-path path  Path where the library, header and generated files
    will be installed to. If missing, /usr/local/lib is assumed.
  --build-path path  Path used to build the library.
    If missing, $(pwd)/../build is assumed.
  -l, --library-type type  Type of library to build and install
    Where 'type' can be either 'static' or 'shared'.

_EOF_
  return
}
